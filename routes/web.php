<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');

Route::get('about', 'CompanyDataController@index')->name('about');
Route::post('about', 'MessagesController@create')->name('sendMessageAbout');

Route::get('contacts', 'CompanyDataController@contacts')->name('contacts');
Route::post('contacts', 'MessagesController@create')->name('sendMessageContact');

Route::get('projects', 'ProjectsController@index')->name('projects');

Route::get('order', 'Constructor\OrdersController@index')->name('order');
Route::post('order', 'Constructor\OrdersController@parse');
Route::post('order/individual', 'MessagesController@create')->name('orderIndividual');

Auth::routes();

Route::group(['middleware'=>['auth', 'isAdministration'], 'prefix'=>'admin'], function () {

	Route::get('/', function() {
		//return redirect()->action('OrdersController@index');
		return view('admin.dashboard');
	})->name('admin');

	Route::get('about', 'CompanyDataController@admin')->middleware(['isAdmin']);
	Route::post('about', 'CompanyDataController@update')->name('adminAboutUpdate');
	
	Route::resource('projects', 'ProjectsController', ['middleware' => 'isAdmin']);
	Route::get('projects', 'ProjectsController@indexAdmin')->middleware(['isAdmin']);

	Route::resource('messages', 'MessagesController', ['middleware' => 'isManager']);
	Route::resource('clients', 'ClientsController', ['middleware' => 'isManager']);
	Route::resource('orders', 'OrdersController');
	Route::resource('users', 'UsersController', ['middleware' => 'isAdmin']);

	//конструктор заказа
	Route::group(['prefix' => 'constructor', 'namespace' => 'Constructor', 'middleware' => ['isAdmin']], function() {

		Route::resource('constructor', 'StepsController');
		Route::get('/', 'StepsController@index')->name('constructor');

		Route::resource('site-types', 'SiteTypesController');
		Route::resource('sections', 'SectionsController');
		Route::resource('elements', 'ElementsController');
		Route::get('elements/create/{type?}', 'ElementsController@create');
	});
});

Route::group(['middleware' => ['auth', 'isClient'], 'prefix'=>'home', 'namespace' => 'Home'], function() {
	Route::get('/', 'HomeController@index')->name('home');

	Route::resource('orders', 'OrdersController');
	Route::get('personal', 'PersonalDataController@index');
	Route::post('personal', 'PersonalDataController@update')->name('personalDataUpdate');
	
});
