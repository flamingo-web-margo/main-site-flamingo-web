<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';
    protected $fillable = ['title', 'description', 'img', 'link', 'id_order_type', 'id_site_type', 'date_created'];

    public function orderType() {
    	return $this->belongsTo('App\OrderType', 'id_order_type');
	}

	public function siteType() {
    	return $this->belongsTo('App\SiteType', 'id_site_type');
	}
}
