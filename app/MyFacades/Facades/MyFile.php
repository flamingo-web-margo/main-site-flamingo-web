<?php
namespace app\MyFacades\Facades;
use Illuminate\Support\Facades\Facade;

class MyFile extends Facade {
   protected static function getFacadeAccessor() { return 'myFile'; }
}