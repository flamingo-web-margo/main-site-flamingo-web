<?php

namespace App\MyFacades;
use Session;

class MyFile
{
    public function upload($img, $root = 'uploads/') {
        $date = date('Y-m-d');
        $rootPath = public_path($root);
        $ext = $img->getClientOriginalExtension();
        $imageSize = getimagesize($img);

        if ($ext != 'jpg' && $ext != 'png' && $ext != 'svg') {
            Session::flash('error', "Расширение - $ext. Изображение должно быть с расширением JPG, PNG или SVG.");
            return false;
        }
        
        if ($img->getSize() > 3000000) {
            Session::flash('error', 'Изображение должно весить не более 3Mb');
            return false;
        }

        @mkdir($rootPath.$date);

        $f_name = $img->getClientOriginalName();
        $f_name = iconv(mb_detect_encoding($f_name), 'WINDOWS-1251', str_replace(' ', '-', $f_name));

        while (file_exists("$rootPath$date/$f_name")) {
            $f_name = rand(0, 1000000).$f_name;
        }

        $img->move($rootPath.$date, $f_name);

        return iconv('WINDOWS-1251', 'UTF-8', "$root$date/$f_name");
    }

    public function delete() {

    }
}
