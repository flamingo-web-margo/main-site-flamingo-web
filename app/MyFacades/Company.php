<?php
namespace App\MyFacades;

use App\CompanyData;

class Company {

	public function info() {
      return CompanyData::first();
   	}

   public function social() {
      return unserialize(CompanyData::first()->social_links);
   }
}