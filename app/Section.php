<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'sections';
    protected $fillable = ['title', 'description'];

    public function config() {
    	return $this->hasMany('App\SectionConfig', 'id_section');
	}

	public function forms() {
    	return $this->belongsToMany('App\SiteForm', 'section_config', 'id_section', 'id_form')
    				  ->withPivot('id', 'id_elements', 'id_elements_default', 'id_elements_block');
  	}
}
