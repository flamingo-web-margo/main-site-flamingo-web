<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteType extends Model
{
    protected $table = 'site_types';
    protected $fillable = ['title', 'description'];

   	public function config() {
		return $this->hasMany('App\SiteConfig', 'id_type');
	}

	public function projects() {
		return $this->hasMany('App\Project', 'id_site_type');
	}

	public function forms() {
    	return $this->belongsToMany('App\SiteForm', 'site_config', 'id_type', 'id_form')
    				      ->withPivot('id')
    	;
  }
}
