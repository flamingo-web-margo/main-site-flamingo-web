<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElementConfig extends Model
{
    protected $table = 'element_config';
    protected $fillable = ['main', 'sort', 'id_form', 'img', 'price'];

    public function siteForm() {
    	return $this->belongsTo('App\SiteForm', 'id_form');
	}

	public function element() {
    	return $this->belongsTo('App\Element', 'id_element');
	}
}
