<?php

namespace App\Providers;

use App;
use Illuminate\Support\ServiceProvider;


class CompanyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
      App::bind('company',function() {
         return new \App\MyFacades\Company;
      });
    }
}
