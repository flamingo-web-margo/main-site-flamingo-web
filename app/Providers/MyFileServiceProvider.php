<?php

namespace App\Providers;

use App;
use Illuminate\Support\ServiceProvider;

class MyFileServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('myFile', function() {
         return new \App\MyFacades\MyFile;
      });
    }
}
