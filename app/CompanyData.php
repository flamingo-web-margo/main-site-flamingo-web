<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyData extends Model
{
    protected $table = 'company_data';
    protected $fillable = ['description', 'tel', 'email', 'copyright', 'social_links'];
}
