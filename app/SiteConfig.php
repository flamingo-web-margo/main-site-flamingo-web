<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteConfig extends Model
{
    protected $table = 'site_config';
    protected $fillable = ['id_type', 'id_form','id_sections', 'id_sections_default', 'id_sections_block'];

    public function orders() {
    	return $this->hasMany('App\OrderConfig', 'id_site_config');
  	}

    public function siteType() {
    	return $this->belongsTo('App\SiteType', 'id_type');
  	}

  	public function form() {
    	return $this->belongsTo('App\SiteForm', 'id_form');
  	}
}
