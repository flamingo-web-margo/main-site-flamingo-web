<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserRole;

class UsersController extends Controller
{

    private $users;

    public function __construct() {
        $this->roles = UserRole::where('name', '!=', 'client')->get();
        $this->users = User::whereIn('id_role', $this->roles->pluck('id'))->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.index')->with(['users' => $this->users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create')->with(['roles' => $this->roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'id_role' => 'required',
            'email' => 'required|string|email|max:255|unique:users'
        ]);

        $requestAll = $request->all();
        $pass = str_random(12);
        $requestAll['password'] = bcrypt($pass);

        User::create($requestAll);

        return back()->with('message', "Пользователь создан.<br>Логин: {$requestAll['email']}<br>Пароль: $pass");
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.users.edit')->with(['user' => $this->users->find($id), 
                                               'roles' => $this->roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = $this->users->find($id);

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'id_role' => 'required'
        ]);

        $requestAll = $request->all();

        if ($requestAll['email'] != $user->email) {
            $this->validate($request, [
                'email' => 'required|string|email|max:255|unique:users'
            ]);
        }

        if ($request->has('password')) {
            $this->validate($request, [
                'password' => 'required|string|min:6|confirmed',
            ]);
            $requestAll['password'] = bcrypt($requestAll['password']);
        } else {
            $requestAll['password'] = $user->password;
        }

        User::find($id)->update($requestAll);

        return back()->with('message', "Данные пользователя обновлены.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return $user->name;
    }

}
