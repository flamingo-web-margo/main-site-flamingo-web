<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanyData;

class CompanyDataController extends Controller
{
    public function index() {
    	return view('about');
    }

    public function contacts() {
    	return view('contacts');
    }

    public function admin() {
    	return view('admin.about');
    }

    public function update(Request $request) {
    	$requestAll = $request->all();

        $requestAll['social_links']['vk'] = $requestAll['vk'];
        $requestAll['social_links']['insta'] = $requestAll['insta'];
        $requestAll['social_links'] = serialize($requestAll['social_links']);
        CompanyData::first()->update($requestAll);
        return back()->with('message', 'Данные успешно обновлены');
    }
}
