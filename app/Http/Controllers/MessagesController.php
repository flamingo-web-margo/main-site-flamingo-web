<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Message;
use Validator;

class MessagesController extends Controller
{
    private $messages;

    public function __construct() {
        $this->messages = Message::orderBy('created_at', 'desc')->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.messages.index')->with(['messages' => $this->messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'name'=>'required|max:255',
            'email'=>'required|email|max:255',
            'text'=>'required'
        ]);

        if ($validator->fails()) {
            return [
                'type' => 'error',
                'msg' => 'Ошибка, сообщение не отправлено.'
            ];
        }
        $requestAll = $request->all();
        if ($request->has('individual')) $requestAll['text'] = 'Индивидуальный заказ: '.$requestAll['text'];

        Message::create($requestAll);

        return [
            'type' => 'success',
            'msg' => 'Сообщение отправлено. Мы ответим вам в ближайшее время.'
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = $this->messages->find($id);
        $messages = $this->messages->whereIn('email', $message->email);

        return view('admin.messages.show')->with(['message' => $message,
                                                  'messages' => $messages,
                                                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
