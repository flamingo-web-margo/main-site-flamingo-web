<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

class PersonalDataController extends Controller
{
    public function index() {
    	return view('home.personal')->with(['user' => Auth::user()]);
    }

    public function update(Request $request) {


        $messages = [
          'name.required' => 'Введите имя',
          'name.max' => 'Слишком длинное имя',
          'email.required' => 'Введите email',
          'email.email' => 'Введите email',
          'email.max' => 'Слишком длинный email'
          
        ];

    	$this->validate($request, [
            'name' => 'required|max:255',
            'tel' => 'max:255',
            'email' => 'required|email|max:255'
        ], $messages);


        if ($request->has('dob')) {
            $this->validate($request, [
            'dob' => 'before:'.date('Y-m-d'),
        ], [
            'dob.before' => 'Вы что из будущего?'
        ]);
        }
        

    	$requestAll = $request->all();
    	$user = Auth::user();

    	if ($request->has('password')) {

    		$this->validate($request, [
    			'password' => 'required|string|min:6|confirmed',
        	],[
                'password.min' => 'Не меньше 6 символов',
                'password.confirmed' => 'Не правильно повторён пароль',
            ]);

    		$requestAll['password'] = bcrypt($requestAll['password']);

    	} else {
    		$requestAll['password'] = $user->password;
    	}

    	$user->update($requestAll);
    	return back()->with(['message' => 'Данные успешно обновлены']);
    }
}
