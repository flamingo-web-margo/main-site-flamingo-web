<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderStep;

class WelcomeController extends Controller
{
    public function index() {
    	$steps = OrderStep::orderBy('number')->get();
    	return view('welcome')->with(['steps' => $steps]);
    }
}
