<?php

namespace App\Http\Controllers\Constructor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use MyFile;
use Auth;
use Mail;

use App\OrderStep;
use App\SiteType;
use App\SiteConfig;
use App\SectionConfig;
use App\ElementConfig;
use App\Order;
use App\OrderConfig;
use App\User;

class OrdersController extends Controller
{
	public function __construct() {
		$this->steps = OrderStep::where('number', '!=', '1')->orderBy('number')->get();
		$this->siteTypes = SiteType::get();
		$this->siteConfig = SiteConfig::get();
	}

	public function index() {

		$elementsMain = ElementConfig::where('main', '!=', null)->get();

		foreach ($this->siteConfig as $config) {
			$config->price = $this->calculatePrice('siteConfig', $config)
							+ $elementsMain->where('main', 'header')->pluck('price')->first()
							+ $elementsMain->where('main', 'footer')->pluck('price')->first();
		}

		return view('order.index')->with([
			'steps' => $this->steps,
			'configs' => $this->siteConfig
		]);
	}

	public function parse(Request $request) {
		switch ($request->type) {
			case 'sections':
				return $this->getSections($request->idSiteConfig);
				break;
			case 'elements':
				return $this->getElements($request->idSiteConfig, json_decode( $request->sectionsAndElements, true), $request->first);
				break;
			case 'order':
				return $this->newOrder($request);
				break;
		}
	}

	private function getSections($idSiteConfig) {
		$config = $this->siteConfig->find($idSiteConfig);
		$sectionsConfig = SectionConfig::whereIn('id', unserialize($config->id_sections))->orderBy('sort')->get();



		foreach ($sectionsConfig as $configSection) {

			$configSection->price = $this->calculatePrice('section', $configSection);

			$configSection->block = in_array($configSection->id, unserialize($config->id_sections_block));
			$configSection->default = in_array($configSection->id, unserialize($config->id_sections_default));

			$configSection->elements = ElementConfig::whereIn('id', unserialize($configSection->id_elements_default))->orderBy('sort')->get();
		}

		$sectionsReturn = SectionConfig::whereIn('id', unserialize($config->id_sections_default))->get();
		$sectionsReturn->elementsMain = ElementConfig::where('main', '!=', null)->get();
		
		
		return [
			'view' => view('order.sections')->with(['step' => $this->steps->where('number', 3)->first(), 
													'configs' => $sectionsConfig])->render(),
			'config' => $this->getConfigsReturn($sectionsReturn)
		];
	}

	private function getElements($idSiteConfig, $sectionsAndElements, $first) {
		
		$siteConfig = SiteConfig::find($idSiteConfig);

		$sectionsConfig = SectionConfig::whereIn('id', array_keys($sectionsAndElements))->orderBy('sort')->get();

		foreach ($sectionsConfig as $configSection) {

			$configSection->elementsConfig = ElementConfig::whereIn('id', unserialize($configSection->id_elements))->orderBy('sort')->get();

			foreach ($configSection->elementsConfig as $config) {
				$config->default = in_array($config->id, unserialize($configSection->id_elements_default));
				if (!empty($configSection->id_elements_block)) {
						$config->block = in_array($config->id, unserialize($configSection->id_elements_block));
				}
			}
		}

		$sectionsConfig->elementsMain = ElementConfig::where('main', '!=', null)->get();

		if ($first) {
			return [
				'view' => view('order.elements')->with(['step' => $this->steps->where('number', 4)->first(), 
														'elementsMain' => $sectionsConfig->elementsMain,
														'sectionsConfig' => $sectionsConfig,
														'siteConfig' => $siteConfig])->render(),
				'price' => $this->calculatePrice('order', json_decode($this->getConfigsReturn($sectionsConfig))),
				'config' => $this->getConfigsReturn($sectionsConfig),
				//в первый запрос $this->getConfigsReturn($sectionsConfig), а второй $sectionsAndElements
				'1'=>$sectionsAndElements,
				'2'=>$sectionsConfig
			];
		} else {
			return [
				'view' => view('order.elements')->with(['step' => $this->steps->where('number', 4)->first(),
														'elementsMain' => ElementConfig::where('main', '!=', null)->get(),
														'sectionsConfig' => $sectionsConfig,
														'siteConfig' => $siteConfig])->render(),
				'price' => $this->calculatePrice('order', $sectionsAndElements),
				'config' => json_encode($sectionsAndElements),
				//в первый запрос $this->getConfigsReturn($sectionsConfig), а второй $sectionsAndElements
				'1'=>$sectionsAndElements,
				'2'=>$sectionsConfig
			];
		}

	}

	public function newOrder(Request $request) {

		$view = 'exists';
		$with = [];
		$requestAll = $request->all();
		$emailExists = User::where('email', $requestAll['email'])->first();

		if ($request->hasFile('logotype')) {
			$requestAll['logotype'] = MyFile::upload($request->file('logotype'), 'orders-logo/');
			if ($requestAll['logotype'] == false) $requestAll['logotype'] = null;
		}

		//если пользователь авторизирован
		if (Auth::check()) {

			$requestAll['id_client'] = Auth::user()->id;

		} elseif ($emailExists) {

			$requestAll['id_client'] = $emailExists->id;

		} else {
			$requestAll['password'] = str_random(12);

			$requestAll['id_client'] = self::createUser([
				'id_role' => '4',
				'name' => $requestAll['name'],
				'email' => $requestAll['email'],
				'password' => $requestAll['password']
			]);

			$view = 'create';
			$with = ['login' => $requestAll['email'],
					 'password' => $requestAll['password']];
		}
		$dataOrder = OrdersController::create($requestAll);

		return [
			'view' => view("order.$view")->with($with)->render(),
			'price' =>  $dataOrder['price']
		];
		
	}

	private function create($request) {

		$request['price'] = $this->calculatePrice('order', json_decode($request['detail_config']));
		$request['detail_config'] = serialize(json_decode($request['detail_config']));
		$request['performed'] = 0;

		$config = OrderConfig::create($request)->id;
		OrderConfig::find($config)->order()->create($request);

		return ['price' => $request['price']];
	}

	public function createUser($data) {
		//$password = $data['password'];

		$data['password'] = bcrypt($data['password']);
		$idUser =  User::create($data)->id;

		// $user = User::find($idUser);
		

		// Mail::send('emails.confirmation', ['user' => $user, 'password' => $password], function ($m) use ($user) {
		//   $m->from('hello@gmail.com', 'Your Application');

		//   $m->to($user->email, $user->name)->subject('Your Reminder!');
		// });
		return $idUser;
	}

	private function getConfigsReturn($sectionsConfig) {

		$configReturn['0'] = [$sectionsConfig->elementsMain->where('main', 'header')->pluck('id')->first(), $sectionsConfig->elementsMain->where('main', 'footer')->pluck('id')->first()];
		foreach ($sectionsConfig as $configSection) {
			$configReturn[$configSection->id] = unserialize($configSection->id_elements_default);
		}
		return json_encode($configReturn);
	}

	public function calculatePrice($type, $data) {
		$price = 0;

		switch($type) {
			case 'siteConfig':
				$sectionsConfig = SectionConfig::whereIn('id', unserialize($data->id_sections_default))->get();
				foreach ($sectionsConfig as $config) {
					$price += ElementConfig::whereIn('id', unserialize($config->id_elements_default))->sum('price');
				}
				break;

			case 'section':
				$elementsDefault = ElementConfig::whereIn('id', unserialize($data->id_elements_default))->get();
				$price = $elementsDefault->sum('price');
				break;

			case 'order':

				foreach ($data as $sectionConfig => $elementsConfig) {
					$price += ElementConfig::whereIn('id', $elementsConfig)->sum('price');
				}
				break;
		}
		return $price;
	}

}
