<?php

namespace App\Http\Controllers\Constructor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\SectionConfig;
use App\Section;
use App\ElementConfig;
use App\Element;
use App\SiteForm;
use MyFile;

class SectionsController extends Controller
{

    private $sections;

    public function __construct() {
        $this->sections = Section::get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.constructor.sections.index')->with(['sections' => $this->sections]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.constructor.sections.create')->with(['siteForms' => SiteForm::get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $configs = array();

        foreach ($request->input('id_form') as $id_form) {

            $requestAll = $request->all();

            $requestAll['id_form'] = $id_form;
            $requestAll['sort'] = $requestAll["sort-{$id_form}-form"];

            $requestAll['id_elements'] = ($request->has("elems_all_$id_form")) ? serialize($requestAll["elems_all_$id_form"]) : '';
            $requestAll['id_elements_default'] = ($request->has("elems_default_$id_form")) ? serialize($requestAll["elems_default_$id_form"]) : '';
            $requestAll['id_elements_block'] = ($request->has("elems_block_$id_form")) ? serialize($requestAll["elems_block_$id_form"]) : '';

            array_push($configs, new SectionConfig($requestAll));
        }

        $section = Section::create($requestAll)->id;
        Section::find($section)->config()->saveMany($configs);

        return back()->with('message', 'Раздел успешно создан');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $config = SectionConfig::find($id);
        $form = SiteForm::find($config->id_form);

        $config->id_elements = unserialize($config->id_elements);
        $config->id_elements_default = !empty($config->id_elements_default) ? unserialize($config->id_elements_default) : [];
        $config->id_elements_block = !empty($config->id_elements_block) ? unserialize($config->id_elements_block) : [];

        return view('admin.constructor.sections.edit')->with(['config' => $config,
                                                              'form' => $form,
                                                            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $requestAll = $request->all();

        $requestAll['id_elements'] = ($request->has("elems_all")) ? serialize($requestAll["elems_all"]) : '';
        $requestAll['id_elements_default'] = ($request->has("elems_default")) ? serialize($requestAll["elems_default"]) : '';
        $requestAll['id_elements_block'] = ($request->has("elems_block")) ? serialize($requestAll["elems_block"]) : '';



        $config = SectionConfig::find($id);
        $config->update($requestAll);

        $config->section()->update($requestAll);
        $config->save();



        return back()->with('message', 'Раздел успешно обновлён');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $config = SectionConfig::find($id);
        $title = $config->section->title;
        $config->delete();
        return $title;
    }
}
