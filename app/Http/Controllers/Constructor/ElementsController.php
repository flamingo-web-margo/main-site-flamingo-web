<?php

namespace App\Http\Controllers\Constructor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ElementConfig;
use App\Element;
use App\SiteForm;
use MyFile;

class ElementsController extends Controller
{

	public function __construct() {
		$this->elementsConfig = ElementConfig::get();
		$this->siteForms = SiteForm::get();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('admin.constructor.elements.index')->with(['elements' => Element::with('config')->paginate(10)]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create($type = null)
	{

		if ($type == 'main') {
			return view('admin.constructor.elements.createMainElem')->with(['siteForms' => $this->siteForms]);
		}

		return view('admin.constructor.elements.create')->with(['siteForms' => $this->siteForms]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$configs = array();
		$requestAll = $request->all();

		if ($request->has('main')) {

			if ($request->hasFile("img")) {
					$requestAll['img'] = MyFile::upload($request->file("img"), 'elements/');
					if ($requestAll['img'] === false) {
						return back();
					}
			}
			array_push($configs, new ElementConfig($requestAll));
		} else {		

			foreach ($request->input('id_form') as $id_form) {

				$requestAll = $request->all();

				$requestAll['id_form'] = $id_form;
				$requestAll['price'] = $requestAll["price-$id_form-form"];
				$requestAll['sort'] = $requestAll["sort-$id_form-form"];

				if ($request->hasFile("img-$id_form-form")) {
					$requestAll['img'] = MyFile::upload($request->file("img-$id_form-form"), 'elements/');
					if ($requestAll['img'] === false) {
						return back();
					}
				}
				array_push($configs, new ElementConfig($requestAll));
			}
		}

		$element = Element::create($request->all())->id;
		Element::find($element)->config()->saveMany($configs);

		return back()->with('message', 'Элемент успешно создан');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		return view('admin.constructor.elements.edit')->with(['config' => ElementConfig::find($id)]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$requestAll = $request->all();

		if ($request->hasFile('img')) {
			$requestAll['img'] = MyFile::upload($request->file('img'), 'elements/');
			if ($requestAll['img'] === false) {
				return back();
			}
		} else {
			$requestAll['img'] = $this->elementsConfig->find($id)->img;
		}

		
		$elementConfig = ElementConfig::find($id);
		$elementConfig->update($requestAll);

		$elementConfig->element()->update($requestAll);
		$elementConfig->save();

		return back()->with('message', 'Данные успешно обновлены');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		
		$elementConfig = ElementConfig::find($id);
		$elementCount = ElementConfig::where('id_element', $elementConfig->id_element)->get()->count();
		$elementTitle = $elementConfig->element->title;

		$elementConfig->delete();
		
		if ($elementCount <= 1) {
			Element::find($elementConfig->id_element)->delete();
		}

		return $elementTitle;
	}
}
