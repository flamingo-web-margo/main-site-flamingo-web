<?php

namespace App\Http\Controllers\Constructor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\SiteType;
use App\SiteConfig;
use App\SiteForm;

class SiteTypesController extends Controller
{

    private $types;

    public function __construct() {
        $this->types = SiteType::get();

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.constructor.site_types.index')->with(['types' => $this->types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.constructor.site_types.create')->with(['siteForms' => SiteForm::get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $configs = array();

        foreach ($request->input('id_form') as $id_form) {

            $requestAll = $request->all();

            $requestAll['id_form'] = $id_form;
            $requestAll['description'] = serialize(($requestAll['description']));

            $requestAll['id_sections'] = ($request->has("sections_all_$id_form")) ? serialize($requestAll["sections_all_$id_form"]) : '';
            $requestAll['id_sections_default'] = ($request->has("sections_default_$id_form")) ? serialize($requestAll["sections_default_$id_form"]) : '';
            $requestAll['id_sections_block'] = ($request->has("sections_block_$id_form")) ? serialize($requestAll["sections_block_$id_form"]) : '';

            array_push($configs, new SiteConfig($requestAll));
        }

        $siteType = SiteType::create($requestAll)->id;
        SiteType::find($siteType)->config()->saveMany($configs);

        return back()->with('message', 'Тип сайта успешно создан');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $config = SiteConfig::find($id);
        $form = SiteForm::find($config->id_form);

        $config->description = unserialize($config->siteType->description);
        $config->id_sections = unserialize($config->id_sections);
        $config->id_sections_default = !empty($config->id_sections_default) ? unserialize($config->id_sections_default) : [];
        $config->id_sections_block = !empty($config->id_sections_block) ? unserialize($config->id_sections_block) : [];

        return view('admin.constructor.site_types.edit')->with([
            'config'=>$config, 
            'form' => $form
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $requestAll = $request->all();

        $requestAll['description'] = serialize(($requestAll['description']));
        $requestAll['id_sections'] = ($request->has("sections_all")) ? serialize($requestAll["sections_all"]) : '';
        $requestAll['id_sections_default'] = ($request->has("sections_default")) ? serialize($requestAll["sections_default"]) : '';
        $requestAll['id_sections_block'] = ($request->has("sections_block")) ? serialize($requestAll["sections_block"]) : '';

        $config = SiteConfig::find($id);
        $config->update($requestAll);

        $config->siteType()->update($requestAll);
        $config->save();

        return back()->with('message', 'Тип сайта успешно обновлён');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $config = SiteConfig::find($id);
        $title = $config->siteType->title;
        $config->delete();
        return $title;
    }

}
