<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;
use App\SiteType;
use App\OrderType;
use MyFile;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $projects;
    private $siteTypes;

    public function __construct() {
        $this->projects = Project::orderBy('created_at', 'desc')->get();
        $this->siteTypes = SiteType::get();
        $this->orderTypes = OrderType::get();
    }

    public function index()
    {
        return view('projects')->with([
            'projects' => $this->projects,
            'siteTypes' => $this->siteTypes,
            'orderTypes' => $this->orderTypes
        ]);
    }

    public function indexAdmin()
    {
        return view('admin.projects.index')->with(['projects' => $this->projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.projects.create')->with(['projects' => $this->projects, 
                                                    'orderTypes' => $this->orderTypes, 
                                                    'siteTypes' => $this->siteTypes
                                                    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'img' => 'required',
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'link' => 'required|url|string|max:255',
            'id_order_type' => 'required',
            'id_site_type' => 'required',
            'date_created' => 'required'
        ]);

        $requestAll = $request->all();

        if ($request->hasFile('img')) {
            $requestAll['img'] = MyFile::upload($request->file('img'));
            if ($requestAll['img'] === false) {
                dump($request->input('img'));
            }
        }

        Project::create($requestAll);
        return back()->with('message', 'Проект успешно создан');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.projects.edit')->with([
            'project' => $this->projects->find($id), 
            'siteTypes' => $this->siteTypes,
            'orderTypes' => $this->orderTypes
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'link' => 'required|url|string|max:255',
            'id_order_type' => 'required',
            'id_site_type' => 'required',
            'date_created' => 'required'
        ]);

        $requestAll = $request->all();

        if ($request->hasFile('img')) {
            $requestAll['img'] = MyFile::upload($request->file('img'));
            if ($requestAll['img'] === false) {
                return back();
            }
        } else {
            $requestAll['img'] = $this->projects->find($id)->img;
        }
        
        Project::find($id)->update($requestAll);
        return back()->with('message', 'Данные успешно обновлены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->delete();

        return $project->title;
    }
}
