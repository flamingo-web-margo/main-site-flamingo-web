<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderType extends Model
{
    protected $table = 'order_types';
    protected $fillable = ['title', 'description'];

    public function orders() {
    	return $this->hasMany('App\Order', 'id_order_type');
	}

	public function projects() {
    	return $this->hasMany('App\Project', 'id_order_type');
	}
}
