<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionConfig extends Model
{
	protected $table = 'section_config';
	protected $fillable = ['sort', 'id_section', 'id_form', 'id_elements', 'id_elements_default', 'id_elements_block'];

	public function siteForm() {
    	return $this->belongsTo('App\SiteForm', 'id_form');
  	}

	public function section() {
    	return $this->belongsTo('App\Section', 'id_section');
  	}
}
