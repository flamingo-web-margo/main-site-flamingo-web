<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = 'orders';
	protected $fillable = ['id_order_type', 'id_client', 'id_config', 'price', 'performed', 'date_start', 'date_end'];

	public function type() {
		return $this->belongsTo('App\OrderType', 'id_order_type');
	}

	public function client() {
		return $this->belongsTo('App\User', 'id_client');
	}

	public function config() {
		return $this->belongsTo('App\OrderConfig', 'id_config');
	}

	public function getDates() {
		return array('created_at', 'date_start', 'date_end');
	}
}
