<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_role', 'name', 'tel', 'email', 'dob', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id_role', 'password', 'remember_token',
    ];

    public function orders() {
        return $this->hasMany('App\Order', 'id_client');
    }

    public function role() {
        return $this->belongsTo('App\UserRole', 'id_role');
    }
}
