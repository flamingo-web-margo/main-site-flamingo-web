<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteForm extends Model
{
    protected $table = 'site_forms';
    protected $fillable = ['title', 'description'];


    public function sites() {
    	return $this->hasMany('App\SiteConfig', 'id_form');
	}

	public function sectionConfig() {
    	return $this->hasMany('App\SectionConfig', 'id_form');
	}

    public function sections() {
        return $this->belongsToMany('App\Section', 'section_config', 'id_form', 'id_section')->withPivot('id');
    }

    public function elementConfig () {
    	return $this->hasMany('App\ElementConfig', 'id_form');
	}

    public function elements() {
        return $this->belongsToMany('App\Element', 'element_config', 'id_form', 'id_element')->withPivot('img', 'price', 'id');
    }

    
	
}
