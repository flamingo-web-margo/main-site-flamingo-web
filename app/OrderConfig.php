<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderConfig extends Model
{
    protected $table = 'order_config';
    protected $fillable = ['description', 'sites_like', 'logotype', 'adaptive', 'mockup', 'id_site_config', 'detail_config'];

    public function order() {
    	return $this->hasMany('App\Order', 'id_config');
  	}

  	public function site() {
    	return $this->belongsTo('App\SiteConfig', 'id_site_config');
	}

	public function get_unser_detail_config()   {
		return unserialize($this->get_attribute('detail_config'));
	}

	public function getDetailConfigAttribute($value) {
		$sectionsAndElements =  (array) unserialize($value);
		$sectionsConfig = SectionConfig::whereIn('id', array_keys($sectionsAndElements))->get();

		foreach ($sectionsAndElements as $idSection => $idEements) {
			if ($idSection == 0) {
				$sectionsConfig->header = ElementConfig::find($idEements[0]);
				$sectionsConfig->footer = ElementConfig::find($idEements[1]);
			} else {
				$sectionsConfig->find($idSection)->id_elements = ElementConfig::whereIn('id', $idEements)->get();
				$sectionsConfig->find($idSection)->price = $sectionsConfig->find($idSection)->id_elements->sum('price');
			}
		}
		return $sectionsConfig;
	}
}
