<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'users_role';
    protected $fillable = ['name', 'title'];

    public function users() {
    	return $this->hasMany('App\User', 'id_role');
    }
}
