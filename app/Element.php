<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Element extends Model
{
    protected $table = 'elements';
    protected $fillable = ['title', 'description'];

    public function config() {
    	return $this->hasMany('App\ElementConfig', 'id_element');
	}

	public function forms() {
    	return $this->belongsToMany('App\SiteForm', 'element_config', 'id_element', 'id_form')->withPivot('img', 'price', 'id');
  	}
}
