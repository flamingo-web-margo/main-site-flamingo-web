@extends('layouts.app')

@section('content')

<body>

    @include('layouts.header')

    <main>
        <section id="section-contacts">
            <div class="centering">
                <h2>Связаться с нами</h2>
                <div>
                    <div>
                        <p>Есть вопросы? Пишите! Мы с радостью ответим.</p>
                        <ul class="contact">
                            <li class="phone"><a href="tel:{{ Company::info()->tel }}">{{ Company::info()->tel }}</a></li>
                            <li class="email"><a href="mailto:{{ Company::info()->email }}">{{ Company::info()->email }}</a></li>
                        </ul>
                    </div>
                    <div>
                        <form method="POST" action="{{ route('sendMessageContact') }}" class="form sendMessage">
                            <p></p>

                            {{csrf_field() }}
                            <div>
                                <input id="f-name" type="text" name="name">
                                <label for="f-name">Имя</label>
                            </div>

                            <div>
                                <input id="f-email" type="mail" name="email" required>
                                <label for="f-email">E-mail</label>
                            </div>

                            <div>
                                <textarea id="f-text" name="text" required></textarea>
                                <label for="f-text">Сообщение</label>
                            </div>
                            
                            <button type="submit" class="button">Отправить</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>

    @include('layouts.footer')

@endsection