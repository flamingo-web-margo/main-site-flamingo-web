@extends('layouts.app')

@section('content')

<body class="welcome">

    @include('layouts.header')

    <main>
        <div id="section-steps">
            <ul>
                @foreach ($steps as $step)
                <li>
                    <span>{{ $step->number }}</span>
                    <h2>{{ $step->title }}</h2>
                    <p>{{ $step->description_min }}</p>
                </li>
                @endforeach
                <li>
                    <a href="order">Попробовать</a>
                </li>
            </ul>
        </div>
    </main>

@endsection