<p>{{ $step->description }}</p>

@foreach ($configs as $config)
<article class="{{ ($config->block) ? 'default ' : '' }}{{ ($config->default) ? 'active' : '' }}" data-section-id="{{ $config->id }}">
    <h3>{{ $config->section->title }}</h3>
    <span>{{ $config->price }}руб</span>
    <h4>Базовый функционал:</h4>
    <ul>
        @foreach($config->elements as $configElement)
        <li>{{ $configElement->element->title }}</li>
        @endforeach
    </ul>

    <button data-type="section" data-value="{{ $config->id }}" class="button"
    {{ ($config->block) ? 'disabled' : '' }}>

    @if ($config->block)
        Заблокирован
    @else
        {{ ($config->default) ? 'Отменить' : 'Добавить' }}
    @endif
    
    </button>

</article>
@endforeach
