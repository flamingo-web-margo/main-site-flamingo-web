<p>{{ $step->description }}</p>

<div id="elems-list" class="form" data-open="2">

    <ul class="elems-tab">
        <li data-name="1">Основные элементы</li>
        <li data-name="2" class="active">Разделы</li>
    </ul>

    <!-- Основные элементы -->
    <ul class="tab" data-tab="1"  data-section-id="0">
        <li><span>Шапка</span>
            <ol>
            @foreach ($elementsMain->where('main', 'header') as $elementConfig)
                <li>
                    <input type="radio" id="elem-{{ $elementConfig->id }}" name="header" value="{{ $elementConfig->id }}" {{ $loop->first ? 'checked' : ''}}>
                    <label for="elem-{{ $elementConfig->id }}">
                        <img src='{{ url("public/{$elementConfig->img}") }}' alt="{{ $elementConfig->element->title }}">
                        <p>{{ $elementConfig->element->description }}</p>
                        <span>{{ $elementConfig->price }}</span>
                    </label>
                </li>
            @endforeach
            </ol>
        </li>
        <li><span>Подвал</span>
            <ol>
            @foreach ($elementsMain->where('main', 'footer') as $elementConfig)
                <li>
                    <input type="radio" id="elem-{{ $elementConfig->id }}" name="footer" value="{{ $elementConfig->id }}" checked>
                    <label for="elem-{{ $elementConfig->id }}">
                        <img src='{{ url("public/{$elementConfig->img}") }}' alt="{{ $elementConfig->element->title }}">
                        <p>{{ $elementConfig->element->description }}</p>
                        <span>{{ $elementConfig->price }}</span>
                    </label>
                </li>
            @endforeach
            </ol>
        </li>
    </ul>

    <!-- Разделы -->
    <ul class="tab" data-tab="2">
    	@foreach ($sectionsConfig as $sectionConfig)
        <li data-section-id="{{ $sectionConfig->id }}"><span>{{ $sectionConfig->section->title }}</span>
            <ol>
            	@foreach ($sectionConfig->elementsConfig as $elementConfig)
                <li class="{{ $elementConfig->block ? 'block' : '' }}">
                    <input type="checkbox" id="elem-{{ $sectionConfig->id }}-{{ $elementConfig->id }}" name="elem[]" value="{{ $elementConfig->id }}"{{ $elementConfig->default ? 'checked' : '' }}>
                    <label for="elem-{{ $sectionConfig->id }}-{{ $elementConfig->id }}">
                        <span>{{ $elementConfig->element->title }}</span>
                        <p>{{ $elementConfig->element->description }}</p>
                        <span>{{ $elementConfig->price }}</span>
                    </label>
                </li>
          		@endforeach
            </ol>
        </li>
        @endforeach
    </ul>
</div>

<div id="elems-mockup" data-site-config-id="{{ $siteConfig->id_form }}">
    <ul data-mockup-section-id="0">
        @foreach ($elementsMain->where('main', 'header') as $elementConfig)
            <li  data-mockup-elem-id="{{ $elementConfig->id }}" class="{{ $loop->first ? 'active' : ''}}"><img src="public/{{ $elementConfig->img }}" alt="Элемент - {{ $elementConfig->element->title }}"></li>
        @endforeach
    </ul>
    
    @if ($siteConfig->id_form == 2) 
        <p>Здесь будет формироваться макет вашего сайта.</p>
        @foreach ($sectionsConfig as $sectionConfig)
        <figure data-mockup-section-id="{{ $sectionConfig->id }}">
            <ul>

                @foreach ($sectionConfig->elementsConfig as $elementConfig)
                <li  data-mockup-elem-id="{{ $elementConfig->id }}" class="{{ ($elementConfig->default || $elementConfig->block) ? 'active' : '' }}"><img src="public/{{ $elementConfig->img }}" alt="Элемент - {{ $elementConfig->element->title }}"></li>
                @endforeach

            </ul>
        </figure>
        @endforeach
    @else
        <figure>
            @foreach ($sectionsConfig as $sectionConfig)
            <ul data-mockup-section-id="{{ $sectionConfig->id }}">
                @foreach ($sectionConfig->elementsConfig as $elementConfig)
                <li data-mockup-elem-id="{{ $elementConfig->id }}" class="{{ ($elementConfig->default || $elementConfig->block) ? 'active' : '' }}"><img src="public/{{ $elementConfig->img }}" alt="Элемент - {{ $elementConfig->element->title }}"></li>
                @endforeach
            </ul>
            @endforeach
        </figure>
    @endif
    <ul data-mockup-section-id="0">
        @foreach ($elementsMain->where('main', 'footer') as $elementConfig)
                    <li  data-mockup-elem-id="{{ $elementConfig->id }}" class="{{ $loop->first ? 'active' : ''}}"><img src="public/{{ $elementConfig->img }}" alt="Элемент - {{ $elementConfig->element->title }}"></li>
        @endforeach
    </ul>
</div>
