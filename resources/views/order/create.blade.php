<div id="user-create">
    <p>Личный кабинет успешно создан. Пароль выслан вам на почту.
    </p>
    <ul>
        <li>Логин: {{ $login }}</li>
        <li>Пароль: {{ $password }}</li>
    </ul>
    <a href="home" class="button">Перейти в личный кабинет</a>
</div>