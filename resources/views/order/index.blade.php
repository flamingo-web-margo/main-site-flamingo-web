@extends('layouts.app')

@section('content')

<body>

    @include('layouts.header')

    <main>
        <section id="section-order"  class="navigation-none" data-open="1" data-token="{{ csrf_token() }}">
            <div class="centering">
                <h2>Формирование заказа</h2>
                <div>
                    <ul id="order-steps">

                        @foreach ($steps as $step)
                        <li onclick="changeStep({{ $step->number - 1 }})">
                            <span >{{ $step->number - 1 }}</span>
                            <span>{{ $step->title }}</span>
                        </li>
                        @endforeach
                    </ul>

                    <!-- Заказ -->
                    <div id="order-list">
                        <!-- Выбор типа сайта -->
                        <div data-section="site-types">
                            <p>{{ $steps['0']->description }}</p>
                            @foreach ($configs as $config)
                            <article data-siteType-id="{{ $config->id }}">
                                <h3>{{ $config->siteType->title }}</h3>
                                <span>{{ $config->price }}руб</span>
                                <h4>Подходит, если вы:</h4>
                                <ul>
                                    @foreach(unserialize($config->siteType->description) as $desc)
                                    <li>{{ $desc }}</li>
                                    @endforeach
                                </ul>
                                <div>
                                    <button data-type="type" data-value="{{ $config->id }}" class="button">Выбрать</button>
                                    <a href="projects" target="_blank">Примеры</a>
                                </div>
                            </article>
                            @endforeach
                        </div>

                        <!-- Определение разделов -->
                        <div data-section="sections"></div>

                        <!-- Настройка элементов -->
                        <div  data-section="elements"></div>

                        <!-- Дополнительная информация -->
                        <div data-section="information" class="form">
                            <p>Заполните дополнительную информацию</p>
                            <div>
                                <textarea form="order-form" name="description" id="f-desc"></textarea>
                                <label for="f-desc">Описание проекта</label>
                            </div>
                            <div>
                                <textarea form="order-form" name="sites_like" id="f-sites-like"></textarea>
                                <label for="f-sites-like">Примеры сайтов, которые нравятся</label>
                            </div>
                            <div>
                                <input form="order-form" type="file" name="logotype" id="f-logo">
                                <label for="f-logo"></label>
                                <p>Если у вас есть логотип, то прикрепите его</p>
                            </div>
                            <div>
                                <input form="order-form" id="f-adapt" type="checkbox" name="adaptive" checked value="1">
                                <label for="f-adapt"><span>Адаптивность</span></label>
                                <p>Включите эту опцию, если хотите, чтобы Ваш сайт подстраивался под размеры экрана планшетов и телефонов.<p>
                            </div>
                            
                        </div>

                        <!-- Оформление заказа -->
                        <div  data-section="set-order">
                        <p>
                            @if (Auth::check())
                            У Вас уже есть личный кабинет и данные внесены, осталось отправить заказ.
                            @else
                            После оформления заказа для вас будет создан личный кабинет
                            @endif
                        </p>
                            <form id="order-form" method="POST" enctype="multipart/form-data" class="form">
                                <p></p>
                                {{ csrf_field() }}
                                <input type="hidden" name="id_order_type" value="1">

                                <div>
                                    @if (Auth::check())
                                    <input type="text" name="name" id="fullname" value="{{ Auth::user()->name }}" readonly>
                                    @else
                                    <input type="text" name="name" id="fullname" value="">
                                    @endif
                                    <label for="fullname">ФИО</label>
                                </div>
                                <div>
                                    @if (Auth::check())
                                    <input type="email" name="email" id="email" value="{{ Auth::user()->email }}" readonly>
                                    @else
                                    <input type="email" name="email" id="email" value="">
                                    @endif
                                    <label for="email">E-mail</label>
                                </div>
                                <button type="submit" class="button">Отправить заказ</button>
                            </form>
                        </div>

                        <div  data-section="set-order-individual">
                            <p>Хотите необычный дизайн? Не нашли нужный элемент? Введите свои контактные данные, а мы свяжемся с вами чтобы узнать все ваши пожелания. Всё просто!</p>

                            <form id="order-form-individual" method="POST" enctype="multipart/form-data" action="{{ route('orderIndividual') }}" class="form">

                                <p></p>

                                {{csrf_field() }}
                                
                                <input type="hidden" name="individual" value="1">
                                <div>
                                    <input id="f-name" type="text" name="name">
                                    <label for="f-name">Имя</label>
                                </div>

                                <div>
                                    <input id="f-email" type="email" name="email" required>
                                    <label for="f-email">E-mail</label>
                                </div>

                                <div>
                                    <textarea id="f-text" name="text" required></textarea>
                                    <label for="f-text">Что-нибудь хотите рассказать?</label>
                                </div>
                                
                                <button type="submit" class="button">Отправить</button>
                            </form>
                        </div>

                        <div class="nav-order">
                            <span onclick="changeStep('prev')">Предыдущий шаг</span>
                            <span onclick="changeStep('next')">Следующий шаг</span>
                        </div>

                    </div>

                    <button onclick="changeStep(6)" class='button btn-individual'>Индивидульный заказ</button>

                    <div id="order-price">
                        <div></div><!-- Заполнение -->
                        <span></span>
                    </div>

                </div>
            </div>
        </section>
    </main>
    <script type="text/javascript" src="{{ asset('public/js/constructor.js') }}"></script>
    @include('layouts.footer')
@endsection