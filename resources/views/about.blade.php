@extends('layouts.app')

@section('content')

<body>

    @include('layouts.header')

    <main>
        <section id="section-about">
            <div class="centering">
                <h2>О веб-студии Flamingo-web</h2>
                    <div>
                    <p>{!! Company::info()->description !!}</p>

                    <h3>Возник вопрос или хотите оставить предложение?<br>Пишите, мы обязательно ответим.</h3>
                    <form method="POST" action="{{ route('sendMessageAbout') }}"  class="form sendMessage">
                        <p></p>

                        {{csrf_field() }}

                        <div>
                            <input id="f-name" type="text" name="name">
                            <label for="f-name">Имя</label>
                        </div>

                        <div>
                            <input id="f-email" type="email" name="email" required>
                            <label for="f-email">E-mail</label>
                        </div>

                        <div>
                            <textarea id="f-text" name="text" required></textarea>
                            <label for="f-text">Сообщение</label>
                        </div>
                        
                        <button type="submit" class="button">Отправить</button>
                    </form>
                </div>
            </div>
        </section>
    </main>

    @include('layouts.footer')

@endsection