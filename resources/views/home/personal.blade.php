@extends('home.layouts.app')

@section('content')

<body>
    @include('home.layouts.header')

    <main>
        <section id="section-about">
            <div class="centering">
                <h2>Личные данные</h2>
                <div>
                	@include('home.layouts.sidebar')
                    <div>
    					<div id="alerts">
    						@if (session('error'))
    							<p>{{ session('error') }}</p>
    						@endif

    						@if (session('message'))
    							<p>{{ session('message') }}</p>
    						@endif
    					</div>

    					<form method="POST" action="{{ route('personalDataUpdate') }}" class="form">

    						{{ csrf_field() }}

    						<div>
        						<input type="text" name="name" value="{{ $user->name }}" id="name-f">
                                <label for="name-f">ФИО</label>
        						@if ($errors->has('name'))
                                <span class="help-block">
                                    {{ $errors->first('name') }}
                                </span>
                                @endif
                            </div>

    						<div>
        						<input type="tel" name="tel" value="{{ $user->tel }}" id="tel-f">
                                <label for="tel-f">Телефон</label>
        						@if ($errors->has('tel'))
                                <span class="help-block">
                                    {{ $errors->first('tel') }}
                                </span>
                                @endif
    						</div>

                            <div>
        						<input type="email" name="email" value="{{ $user->email }}" id="email-f">
                                <label for="email-f">E-mail</label>
        						@if ($errors->has('email'))
                                <span class="help-block">
                                    {{ $errors->first('email') }}
                                </span>
                                @endif
                            </div>

                            <div>
        						<input type="date" name="dob" value="{{ $user->dob }}" id="date-f">
                                <label for="date-f">Дата рождения</label>
                                @if ($errors->has('dob'))
                                <span class="help-block">
                                    {{ $errors->first('dob') }}
                                </span>
                                @endif
                            </div>

                            <div>
        						<input type="password" name="password" id="pass-f-1">
                                <label for="pass-f-1">Пароль</label>
        						@if ($errors->has('password'))
                                    <span class="help-block">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif
                            </div>

                            <div>
        						<input type="password" name="password_confirmation" id="pass-f-2">
                                <label for="pass-f-2">Повторите пароль</label>
                            </div>

    						<button type="submit" class="button">Сохранить</button>
    						
    					</form>
                    </div>
				</div>
            </div>
        </section>
    </main>

    @include('home.layouts.footer')

@endsection