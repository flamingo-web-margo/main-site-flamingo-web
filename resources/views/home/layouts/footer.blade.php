<footer>
    <p><a href="index.html">{!! Company::info()->copyright !!}</a></p>
    <ul class="social">
        <li><a href="{{ Company::social()['vk'] }}" class="vk" target="_blank"></a></li>
        <li><a href="{{ Company::social()['insta'] }}" class="insta" target="_blank"></a></li>
    </ul>
</footer>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
