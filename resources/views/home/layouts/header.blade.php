<header>
    <div>
        <button class="nav-btn">
            <span><span></span></span>
        </button>

        <a href="{{ url('/') }}"><img src="{{ asset('public/images/Flamingo-web.svg') }}" alt="Логотип веб-студии в Ростове-на-Дону Flamingo-web"></a>

        <a href="{{ url('/home') }}" class=
                @if (Auth::guest())
                    "link-home link-login"
                @elseif (Auth::user()->id_role == '4')
                    "link-home link-home-user"
                @else
                    "link-home link-home-admin"
                @endif
        >
        </a>

        <nav>
            <ul>
                <li><a href="{{ route('about') }}">Студия</a></li>
                <li><a href="{{ route('projects') }}">Портфолио</a></li>
                <li><a href="{{ route('order') }}">Заказ</a></li>
                <li><a href="{{ route('contacts') }}">Контакты</a></li>
                <li><a href="{{ url('/home') }}" class=
                @if (Auth::guest())
                    "link-login"
                @elseif (Auth::user()->id_role == '4')
                    "link-home-user"
                @else
                    "link-home-admin"
                @endif
                >
                </a></li>
               
            </ul>
        </nav>
    </div>
</header>