<ul class="sidebar">
	<li><a href="{{ route('home') }}">Главная</a></li>
	<li><a href="{{ route('home') }}\orders">Мои заказы</a></li>
	<li><a href="{{ route('home') }}\personal">Личные данные</a></li>
	<li>
	    <a href="{{ route('logout') }}"
	        onclick="event.preventDefault();
	                 document.getElementById('logout-form').submit();">
	        Выход
	    </a>
	    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	        {{ csrf_field() }}
	    </form>
	</li>
</ul>