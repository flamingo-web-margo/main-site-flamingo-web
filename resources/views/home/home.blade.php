@extends('home.layouts.app')

@section('content')

<body>
    @include('home.layouts.header')

    <main>
        <section id="section-about">
            <div class="centering">
                <h2>Личный кабинет</h2>
                <div>
                    @include('home.layouts.sidebar')
                    <div>
                        <p>Добро пожаловать в личный кабинет, {{ $user->name }}!</p>
                    </div>
                </div>
            </div>
        </section>
    </main>

    @include('home.layouts.footer')

@endsection