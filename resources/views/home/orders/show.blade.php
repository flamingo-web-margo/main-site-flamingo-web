@extends('home.layouts.app')

@section('content')

<body>
	@include('home.layouts.header')

	<main>
		<section id="section-about">
			<div class="centering">
				<h2>Cайт типа "{{ $order->config->site->siteType->title }}"</h2>
				<div>
					@include('home.layouts.sidebar')
					<div>
						<div class="performed">
							<div></div><!-- Заполнение -->
							<span>{{ $order->performed }}%</span>
						</div>
						<table class="data-order">
							<caption>Данные заказа</caption>
							<tr>
								<td>Заказ принят:</td>
								<td>{{ $order->created_at->format('d.m.Y') }}</td>
							</tr>
							<tr>
								<td>Сроки выполнения:</td>
								<td>
									{{ $order->date_start ? $order->date_start->format('d.m.Y') : 'не определено' }} - 
									{{ $order->date_end ? $order->date_end->format('d.m.Y') : 'не определено' }}
								</td>
							</tr>
							<tr>
								<td>Сумма:</td>
							   <td>{{ $order->price }}</td>
							</tr>
							<tr>
								<td>Выполнено:</td>
								<td>{{ $order->performed }}%</td>
							</tr>
						</table>

						<table class="data-order-add">
							<caption>Дополнительные сведения</caption>
							<tr>
								<td>Описание</td>
								<td>{{ $order->config->description }}</td>
							</tr>
							<tr>
								<td>Понравившиеся сайты</td>
								<td>{{ $order->config->sites_like }}</td>
							</tr>
							<tr>
								<td>Логотип</td>
								<td>
									@if ($order->config->logotype != 'null')
									<img src='{{ url("public/{$order->config->logotype}") }}' alt="Логотип заказа">
									@endif
								</td>
							</tr>
							<tr>
								<td>Адаптивность</td>
								<td><div class="
									@if ($order->config->adaptive == 1)
									adaptive true
									@else
									adaptive false
									@endif
									">
									</div>
								</td>
							</tr>                            
						</table>

						<table class="data-order-sections">
							<caption>Сайт типа "{{ $order->config->site->siteType->title }}" с разделами: </caption>
								<tr class="section-head">
									<td colspan="3">Основные элементы</td>
								</tr>
								<tr class="section-body">
									<td>{{ $order->config->detail_config->header->element->title }}</td>
									<td>{{ $order->config->detail_config->header->element->description }}</td>
									<td>{{ $order->config->detail_config->header->price }}руб</td>
								</tr>
								<tr class="section-body">
									<td>{{ $order->config->detail_config->footer->element->title }}</td>
									<td>{{ $order->config->detail_config->footer->element->description }}</td>
									<td>{{ $order->config->detail_config->footer->price }}руб</td>
								</tr>
							@foreach($order->config->detail_config as $sectionConfig)
								<tr class="section-head">
									<td>{{ $sectionConfig->section->title }}</td>
									<td>{{ $sectionConfig->section->description }}</td>
									<td>{{ $sectionConfig->price }}руб</td>
								</tr>
								@foreach($sectionConfig->id_elements as $elementConfig)
								<tr class="section-body">
									<td>{{ $elementConfig->element->title }}</td>
									<td>{{ $elementConfig->element->description }}</td>
									<td>{{ $elementConfig->price }}руб</td>
								</tr>
								@endforeach
							@endforeach
						</table>
						
						<table class="data-order-mockup">
							<caption>{{ ($order->config->site->id_form == 2) ? 'Макеты' : 'Макет' }}</caption>

							@if ($order->config->site->id_form == 2) 
								<tbody>
								@foreach($order->config->detail_config as $sectionConfig)
									<tr>
										<td>
											<span>{{ $sectionConfig->section->title }}</span>
											<ul>
												<li>
													<img src='{{ url("public/{$order->config->detail_config->header->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
												</li>
											@foreach ($sectionConfig->id_elements as $elementConfig)
												<li>
													<img src='{{ url("public/{$elementConfig->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
												</li>
											@endforeach
												<li>
													<img src='{{ url("public/{$order->config->detail_config->footer->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
												</li>
											</ul>
										</td>
									</tr>
								@endforeach
								</tbody>
							@else
								<tbody>
									<tr>
										<td>
										<ul>
											<li>
												<img src='{{ url("public/{$order->config->detail_config->header->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
											</li>
										@foreach($order->config->detail_config as $sectionConfig)
											
											<li>
												<ol>
													@foreach ($sectionConfig->id_elements as $elementConfig)
													<li>
														<img src='{{ url("public/{$elementConfig->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
													</li>
													@endforeach
												</ol>
											</li>
										@endforeach
											<li>
												<img src='{{ url("public/{$order->config->detail_config->footer->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
											</li>
										</ul>
										</td>
									</tr>
								</tbody>

							@endif

						</table>
					</div>
				</div>
			</div>
		</section>
	</main>

	@include('home.layouts.footer')

@endsection