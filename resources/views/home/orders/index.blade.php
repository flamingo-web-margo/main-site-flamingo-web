@extends('home.layouts.app')

@section('content')

<body>
	@include('home.layouts.header')

	<main>
		<section id="section-about">
			<div class="centering">
				<h2>Заказы</h2>
				<div>
					@include('home.layouts.sidebar')
					<div>
						@foreach ($orders as $order)
						<article>
							<h3>{{ $order->type->title }} заказ на сайт типа "{{ $order->config->site->siteType->title }}"</h3>
							<span>Заказ принят:<br>{{ $order->created_at->format('d.m.Y') }}</span><br>
							<span>Сроки выполнения:<br>
							{{ $order->date_start ? $order->date_start->format('d.m.Y') : 'не определено' }} - 
							{{ $order->date_end ? $order->date_end->format('d.m.Y') : 'не определено' }}
							</span>
							<div class="performed">
		                        <div></div><!-- Заполнение -->
		                        <span>{{ $order->performed }}%</span>
		                    </div>
							<span>Сумма заказа: {{ $order->price }}руб</span>
							<a href="{{ action('Home\OrdersController@show', ['order'=>$order->id]) }}"></a>
						</article>
						@endforeach
					</div>
				</div>
			</div>
		</section>
	</main>

	@include('home.layouts.footer')

@endsection