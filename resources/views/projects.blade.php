@extends('layouts.app')

@section('content')

<body>

    @include('layouts.header')

    <main>
        <section id="section-projects">
            <div class="centering">
                <h2>Последние работы</h2>
                <div id="filter-cat">
                    {{--
                    <ul>
                        <li>
                            <ol>
                                <li data-cat="normal" class="active">Обычный</li>
                                <li data-cat="individual">Индивидуальный</li>
                            </ol>
                        </li>
                        <li>
                            <ol>
                                @foreach($siteTypes as $type)
                                    <li data-cat="type-{{ $type->id }}">{{ $type->title }}</li>
                                @endforeach
                            </ol>
                        </li>
                    </ul>
                    --}}

                    @foreach($projects as $project)
                        <a href="{{ $project->link }}" target="_blank">
                            <article>
                                <img src="{{ url('public/'.$project->img) }}" alt="{{ $project->title }}">
                                <div>
                                    <h3>{{ $project->title }}</h3>
                                    <span class="type">{{ $project->orderType->title }} | {{ $project->siteType->title }}</span> |
                                    <span class="date">{{ $project->created_at->format('d-m-Y') }}</span>
                                    <p>{{ $project->description }}</p>
                                    <span class="more">Перейти на сайт</span>
                                </div>
                            </article>
                        </a>
                    @endforeach
                </div>
                {{-- <button id="more-projects" class="button">Посмотреть следующие</button> --}}
            </div>

        </section>
    </main>

    @include('layouts.footer')
@endsection