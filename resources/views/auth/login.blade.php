@extends('layouts.app')

@section('content')
<div id="login" class="centering">
    <h2>Авторизация</h2>
    <form class="form" role="form" method="POST" action="{{ route('login') }}">
        
        @if ($errors->has('email') || $errors->has('password'))
            <p>Данные не верны.</p>
        @endif
        
        {{ csrf_field() }}
        <div>
            <input id="email" type="email" class="filled" name="email" required autofocus>
            <label for="email">E-Mail</label>
            
        </div>

        <div>
            <input id="password" type="password" class="filled" name="password" required>
            <label for="password">Пароль</label>
            @if ($errors->has('password'))
                <span class="help-block">
                    {{ $errors->first('password') }}
                </span>
            @endif
        </div>

        <button type="submit" class="button">Войти</button>
        {{--
        <a href="{{ route('password.request') }}">Забыли пароль?</a>
        --}}
    </form>
</div>
@endsection
