@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Проект - {{ $project->title }}</div>
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>


				<form method="POST" action="{{ action('ProjectsController@update', ['project'=>$project->id]) }}" enctype="multipart/form-data" role="form">

					{{ csrf_field() }}
					{{ method_field('put') }}

					<div class="row">
						<div class="col-md-3">
							<figure>
								<img src="{{ asset('public/'.$project->img) }}" alt="{{ $project->title }}" class="img-responsive">
							</figure>
						</div>
						<div class="col-md-9">
							<div class="form-group">
								<label for="img-f">Превью</label>
							    <input type="file" name="img" id="img-f" class="form-control">

								@if ($errors->has('img'))
							    <span class="text-danger">{{ $errors->first('img') }}</span>
							    @endif
							</div>
						</div>
					</div>
					<br>
					<div class="form-group">
						<label for="title-f">Название</label>
					    <input type="text" name="title" id="title-f" class="form-control" value="{{ $project->title }}">
					    
						@if ($errors->has('title'))
					    <span class="text-danger">{{ $errors->first('title') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="deck-f">Описание</label>
					    <textarea name="description" id="desk-f" class="form-control">{{ $project->description }}</textarea>
					    
						@if ($errors->has('description'))
					    <span class="text-danger">{{ $errors->first('description') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="link-f">Ссылка</label>
					    <input type="url" name="link" id="link-f" class="form-control" value="{{ $project->link }}">
					    
						@if ($errors->has('link'))
					    <span class="text-danger">{{ $errors->first('link') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="order-type-f">Тип заказа</label>
					    <select name="id_order_type" id="order-type-f" class="form-control">
							@foreach ($orderTypes as $type)
							<option value="{{ $type->id }}"
							@if ($project->id_order_type == $type->id)
								selected
							@endif
							>{{ $type->title }}</option>
							@endforeach
						</select>
					    
						@if ($errors->has('id_order_type'))
					    <span class="text-danger">{{ $errors->first('id_order_type') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="site-type-f">Тип сайта</label>
					    <select name="id_site_type" id="site-type-f" class="form-control">
							@foreach ($siteTypes as $type)
							<option value="{{ $type->id }}"
							@if ($project->id_site_type == $type->id)
								selected
							@endif
							>{{ $type->title }}</option>
							@endforeach
						</select>
					    
						@if ($errors->has('id_site_type'))
					    <span class="text-danger">{{ $errors->first('id_site_type') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="date-f">Дата создания</label>
					   	<input type="date" name="date_created" id="date-f" value="{{ $project->date_created }}" class="form-control" >
					    
						@if ($errors->has('date_created'))
					    <span class="text-danger">{{ $errors->first('date_created') }}</span>
					    @endif
					</div>

					<button type="submit" class="btn btn-primary">Сохранить</button>

				</form>
			</div>
		</div>

@endsection