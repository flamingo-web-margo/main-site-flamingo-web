@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Все проекты</div>
			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>
				
				<div class="well well-sm">
					<a href="{{ action('ProjectsController@create') }}" class="btn btn-primary">Создать новый проект</a>
				</div>

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Превью</th>
							<th>Название</th>
							<th>Тип заказа</th>
							<th>Тип сайта</th>
							<th>Дата создания</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach($projects as $project)
						<tr>
							<td><img src="{{ asset('public/'.$project->img ) }}" width="100px" alt="{{ $project->title }}"></td>
							<td><a href="{{ $project->link }}">{{ $project->title }}</a></td>
							<td>{{ $project->orderType->title }}</td>
							<td>{{ $project->siteType->title }}</td>
							<td>{{ $project->date_created }}</td>
							<td>
								<a href="{{ action('ProjectsController@edit', ['project'=>$project->id]) }}" class="btn btn-primary">Изменить</a>
								
								<a href="{{ action('ProjectsController@destroy', ['project'=>$project->id]) }}" data-token="{{ csrf_token() }}" class="btn btn-danger del">Удалить</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

@endsection