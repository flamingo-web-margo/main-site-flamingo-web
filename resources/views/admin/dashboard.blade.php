@extends('admin.layouts.app')

@section('content')

<table class="table table-hover">
  <thead>
  	<tr>
	  	<th>Название</th>
	  	<th>Ссылка</th>
  	</tr>
  </thead>
  <tbody>
  	<tr>
  		<td>Хостинг - beget</td>
  		<td><a href="https://cp.beget.com/" class="btn btn-primary" target="_blank">Перейти</a></td>
  	</tr>
  	<tr>
  		<td>Яндекс.Метрика</td>
  		<td><a href="https://metrika.yandex.ru/dashboard?group=dekaminute&period=today&id=45031534" class="btn btn-primary" target="_blank">Перейти</a></td>
  	</tr>
  	<tr>
  		<td>Яндекс.Почта для домена</td>
  		<td><a href="https://pdd.yandex.ru/domain/flamingo-web.ru/?master=yes" class="btn btn-primary" target="_blank">Перейти</a></td>
  	</tr>
  	<tr>
  		<td>Репозиторий</td>
  		<td><a href="https://bitbucket.org/flamingo-web/main-site-flamingo-web" class="btn btn-primary" target="_blank">Перейти</a></td>
  	</tr>
  </tbody>
</table>

@endsection