@extends('admin.layouts.app')

@section('content')


		<div class="panel panel-primary">
			<div class="panel-heading">Пользователи</div>

			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>

				<div class="well well-sm">
					<a href="{{ action('UsersController@create') }}" class="btn btn-primary">Создать нового пользователя</a>
				</div>

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Тип</th>
							<th>Имя</th>
							<th>Телефон</th>
							<th>E-mail</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
						<tr>
							<td>{{ $user->role->title }}</td>
							<td>{{ $user->name }}</td>
							<td>{{ $user->tel }}</td>
							<td>{{ $user->email }}</td>
							<td>
								<a href="{{ action('UsersController@edit', ['user'=>$user->id]) }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
								
								<a href="{{ action('UsersController@destroy', ['user'=>$user->id]) }}" data-token="{{ csrf_token() }}" class="btn btn-danger del"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

@endsection