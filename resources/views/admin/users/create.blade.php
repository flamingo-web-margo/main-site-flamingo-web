@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Создание пользователя</div>

			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{!! session('message') !!}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<form method="POST" action="{{ action('UsersController@store') }}" enctype="multipart/form-data" role="form">

					{{ csrf_field() }}


					<div class="form-group">
						<label for="name-f">Имя</label>
					    <input type="text" name="name" id="name-f" class="form-control" value="{{ old('name') }}">
					    
						@if ($errors->has('name'))
					    <span class="text-danger">{{ $errors->first('name') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="tel-f">Телефон</label>
					    <input type="tel" name="tel" id="tel-f" class="form-control" value="{{ old('tel') }}">
					    
						@if ($errors->has('tel'))
					    <span class="text-danger">{{ $errors->first('tel') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="email-f">E-mail</label>
					    <input type="email" name="email" id="email-f" class="form-control" value="{{ old('email') }}">
					    
						@if ($errors->has('email'))
					    <span class="text-danger">{{ $errors->first('email') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="role-f">Тип заказа</label>
					    <select name="id_role" id="role-f" class="form-control">
							@foreach ($roles as $role)
							<option value="{{ $role->id }}">{{ $role->title }}</option>
							@endforeach
						</select>
					    
						@if ($errors->has('id_role'))
					    <span class="text-danger">{{ $errors->first('id_role') }}</span>
					    @endif
					</div>

					<button type="submit" class="btn btn-primary">Сохранить</button>

				</form>
			</div>
		</div>

@endsection