<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name') }} | Панель управления</title>

	<!-- Styles -->
	<link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('public/css/admin.css') }}" rel="stylesheet">
	<script type="text/javascript" src="{{ asset('public/js/jquery-3.2.1.min.js') }}"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
				  <a class="navbar-brand" href="{{ url('/') }}">Панель управления {{ config('app.name') }}</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="{{ url('/') }}">На сайт</a></li>
						<li><a href="{{ route('logout') }}" class="link-logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="{{ route('admin') }}">Главная</a></li>
					<li><a href="{{ route('admin') }}/orders">Заказы</a></li>
					@if (Auth::user()->id_role == 1 || Auth::user()->id_role == 2)
					<li><a href="{{ route('admin') }}/clients">Клиенты</a></li>
					<li><a href="{{ route('admin') }}/messages">Сообщения</a></li>
					@endif


					@if (Auth::user()->id_role == 1)
					<li><a href="{{ route('admin') }}/about">Данные компании</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Портфолио <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="{{ route('admin') }}/projects">Все проекты</a></li>
							<li><a href="{{ action('ProjectsController@create') }}">Создать новый проект</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Конструктор заказа <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="{{ route('constructor') }}/constructor">Шаги</a></li>
							<li><a href="{{ route('constructor') }}/site-types">Типы сайтов</a></li>
							<li><a href="{{ route('constructor') }}/sections">Разделы</a></li>
							<li><a href="{{ route('constructor') }}/elements">Элементы</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Пользователи <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="{{ action('UsersController@index') }}">Все пользователи</a></li>
							<li><a href="{{ action('UsersController@create') }}">Создать пользователя</a></li>
						</ul>
					</li>
					@endif
				</ul>
			</div>

			<div class="col-md-10">
				@yield('content')
			</div>
		</div>
	</div>
	

	<!-- Scripts -->
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		{{ csrf_field() }}
	</form>
	<script type="text/javascript" src="{{ asset('public/js/admin.js') }}"></script>
	<script src="{{ asset('public/js/app.js') }}"></script>
</body>
</html>
