@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Шаги формирования заказа</div>
			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>№</th>
							<th>Название</th>
							<th>Описание</th>
							<th>Действие</th>
						</tr>
					</thead>
					<tbody>
						@foreach($steps as $step)
						<tr>
							<td>{{ $step->number }}</td>
							<td>{{ $step->title }}</td>
							<td>{{ $step->description_min }}</td>
							<td>
								<a href="{{ action('Constructor\StepsController@edit', ['step' => $step->id]) }}" class="btn btn-primary form-update"><span class="glyphicon glyphicon-pencil"></span></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
@endsection