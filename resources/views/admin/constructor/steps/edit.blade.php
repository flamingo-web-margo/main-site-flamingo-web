@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Шаг №{{ $step->number }} - {{ $step->title }}</div>
			
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>


				<form method="POST" action="{{ action('Constructor\StepsController@update', ['step'=>$step->id]) }}" enctype="multipart/form-data" role="form">

					{{ csrf_field() }}
					{{ method_field('put') }}


					<input type="hidden" name="number" value="{{ $step->number }}">

					<div class="form-group">
						<label for="title-f">Название</label>
					    <input type="text" name="title" value="{{ $step->title }}" id="title-f" class="form-control">
					    
						@if ($errors->has('title'))
					    <span class="text-danger">{{ $errors->first('title') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="decc-min-f">Коротое описание</label>
					    <textarea name="description_min" id="desc-min-f" class="form-control">{{ $step->description_min }}</textarea>
					    
						@if ($errors->has('description_min'))
					    <span class="text-danger">{{ $errors->first('description_min') }}</span>
					    @endif
					</div>

					<div class="form-group">
						<label for="desc-f">Полное описание</label>
					    <textarea name="description" id="desc-f" class="form-control">{{ $step->description }}</textarea>
					    
						@if ($errors->has('description'))
					    <span class="text-danger">{{ $errors->first('description') }}</span>
					    @endif
					</div>

					<button type="submit" class="btn btn-primary">Сохранить</button>

				</form>
			</div>
		</div>

@endsection