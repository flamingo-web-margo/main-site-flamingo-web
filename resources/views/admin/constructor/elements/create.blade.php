@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Создание элемента</div>
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<form method="POST" action="{{ action('Constructor\ElementsController@store') }}" role="form" class="form"  enctype="multipart/form-data">

					{{ csrf_field() }}


					<div class="form-group">
						<label for="title-f">Название</label>
						<input type="text" name="title" id="title-f"  class="form-control" value="{{ old('title') }}">
					</div>

					<div class="form-group">
						<label for="desc-f">Описание</label>
						<textarea name="description" id="desc-f" class="form-control">{{ old('description') }}</textarea>
					</div>

					<div class="row">
					@foreach($siteForms as $form) 
						<div class="col-md-6">

							<label for="id-form">{{ $form->title }}</label>
							<input type="checkbox" name="id_form[]" value="{{ $form->id }}" id="id-form" {{ $form->id == 2 ? 'checked' : ''}}>
							<br>
							<figure></figure>

							<div class="form-group">
								<label for="img-f-{{ $form->id }}">Изображение</label>
								<input type="file" name="img-{{ $form->id }}-form" id="img-f-{{ $form->id }}" class="form-control">

								@if ($errors->has('img-{{ $form->id }}-form'))
								<span class="text-danger">{{ $errors->first("img-{$form->id}-form") }}</span>
								@endif
							</div>

							<div class="form-group">
								<label for="price-f-{{ $form->id }}">Цена</label>
								<input type="number" min="0" name="price-{{ $form->id }}-form" id="price-f-{{ $form->id }}" class="form-control" value='{{ old("price-{$form->id}-form") }}'>
							</div>

							<div class="form-group">
								<label for="sort-f-{{ $form->id }}">Сортировка</label>
								<input type="number" min="0" name="sort-{{ $form->id }}-form" id="sort-f-{{ $form->id }}"  class="form-control" value="{{ old('sort-{ $form->id }-form') }}">
							</div>
						</div>
						@endforeach 
					</div>

					

					<button type="submit" class="btn btn-primary">Сохранить</button>
					
				</form>
			</div>
		</div>
@endsection