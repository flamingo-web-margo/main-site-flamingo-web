@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">
				Создание основного элемента
			</div>
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<form method="POST" action="{{ action('Constructor\ElementsController@store') }}" role="form" class="form" enctype="multipart/form-data">

					{{ csrf_field() }}

					<div class="form-group">
					    <label for="title-f">Название</label>
					    <input type="text" name="title" id="title-f" value="{{ old('title') }}" class="form-control">
					</div>

					<div class="form-group">
					    <label for="desc-f">Описание</label>
					    <textarea name="description" id="desc-f" class="form-control">{{ old('description') }}</textarea>
					</div>

					<div class="form-group">
						<label for="main-f">Сортировка</label>
						<select name="main" class="form-control">
							<option value="header">Шапка</option>
							<option value="footer">Подвал</option>
						</select>
					</div>

					<div class="form-group">
						<label for="img">Изображение</label>
						<input type="file" name="img" id="img" class="form-control">
					</div>

					<br>
					<div class="form-group">
						<label for="price-f">Цена</label>
						<input type="number" min="0" name="price" id="price-f" class="form-control" value="{{ old('price') }}">
					</div>

					<button type="submit" class="btn btn-primary">Сохранить</button>
					
				</form>
			</div>
		</div>
@endsection