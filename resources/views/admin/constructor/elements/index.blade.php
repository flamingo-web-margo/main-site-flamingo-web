@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Элементы</div>

			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>

				<div class="well well-sm">
				<a href="{{ action('Constructor\ElementsController@create') }}" class="btn btn-primary">Создать элемент</a>
				<a href="{{ action('Constructor\ElementsController@create', ['type' => 'main']) }}" class="btn btn-primary">Создать основной элемент</a>
				</div>

				<table class="table table-bordered items">
					<thead>
						<tr>
							<th>Название и описание</th>
							<th>Вид сайта</th>
							<th>Цена</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>

						@foreach($elements as $element)

						<tr class="name">
							<td rowspan="{{$element->forms->count() + 1}}">{{ $element->title }}<hr>{{ $element->description }}</td>

							
							<td>
								{{ $element->config->first()->main ? 'Для любого вида' : ''}}
							</td>

							<td>{{ $element->config->first()->main ? $element->config->first()->price : ''}}</td>

							<td>
							@if ($element->config->first()->main)
								<a href="{{ action('Constructor\ElementsController@edit', ['config'=>$element->config->first()->id]) }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
								
								<a href="{{ action('Constructor\ElementsController@destroy', ['config'=>$element->config->first()->id]) }}" data-token="{{ csrf_token() }}" class="btn btn-danger del"><span class="glyphicon glyphicon-trash"></span></a>
							@endif
							</td>
							
						</tr>

						@foreach ($element->forms as $form)
						<tr>
							<td>{{ $form->title }}</td>
							<td>{{ $form->pivot->price }}</td>
							<td>
								<a href="{{ action('Constructor\ElementsController@edit', ['config'=>$form->pivot->id]) }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
								
								<a href="{{ action('Constructor\ElementsController@destroy', ['config'=>$form->pivot->id]) }}" data-token="{{ csrf_token() }}" class="btn btn-danger del"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
						</tr>
						@endforeach

						@endforeach
					</tbody>
				</table>

				{{ $elements->links() }}
			</div>
		</div>
@endsection