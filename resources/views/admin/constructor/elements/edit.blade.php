@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">
				Элемент -{{ $config->element->title }} {{ $config->main == 'header' ? 'шапка' : ''}}{{ $config->main == 'footer' ? 'подвал' : '' }}
			</div>
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<form method="POST" action="{{ action('Constructor\ElementsController@update', ['element' => $config->id]) }}" role="form" class="form" enctype="multipart/form-data">

					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class="form-group">
					    <label for="title-f">Название</label>
					    <input type="text" name="title" id="title-f" value="{{ $config->element->title }}" class="form-control">
					</div>

					<div class="form-group">
					    <label for="desc-f">Описание</label>
					    <textarea name="description" id="desc-f" class="form-control">{{ $config->element->description }}</textarea>
					</div>

					@if (!$config->main)
					<input type="hidden" name="id_form" value="{{ $config->siteForm->id }}" id="id-form" checked>
					@endif

					<div class="row">
					
						<div class="col-md-4">
							<figure>
								@if ($config->img) 
								<img src="{{ asset('public/'.$config->img) }}" alt="{{ $config->element->title }}" class="img-responsive">
								@endif
							</figure>
						</div>

						<div class="col-md-8">

							<div class="form-group">
								<label for="img">Изображение</label>
								<input type="file" name="img" id="img" class="form-control">

								@if ($errors->has('img'))
								<span class="text-danger">{{ $errors->first("img") }}</span>
								@endif
							</div>
						</div>
					</div>
					<br>
					<div class="form-group">
						<label for="price-f">Цена</label>
						<input type="number" min="0" name="price" id="price-f" class="form-control" value="{{ $config->price }}">
					</div>
					
					@if (!$config->main)
					<div class="form-group">
						<label for="sort-f">Сортировка</label>
						<input type="number" min="0" name="sort" id="sort-f" class="form-control" value="{{ $config->sort }}">
					</div>
					@endif

					<button type="submit" class="btn btn-primary">Сохранить</button>
					
				</form>
			</div>
		</div>
@endsection