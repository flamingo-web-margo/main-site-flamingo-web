@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Создание типа сайта</div>
			
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<form method="POST" action="{{ action('Constructor\SiteTypesController@store') }}" role="form" class="form">

					{{ csrf_field() }}

					<div class="form-group">
					    <label for="title-f">Название</label>
					    <input type="text" name="title" id="title-f" class="form-control" required>
					</div>

					<fieldset class="panel panel-default">
						<div class="panel-heading">
							Подходит, если вы:
						</div>
						<div class="panel-body">
							<div class="input-group field-desc" data-number="1">
								<input type="text" name="description[]" id="desk-f-1" class="form-control" required>
								<span class="input-group-btn">
									<button class="btn btn-danger del-field" type="button">Удалить</button>
								</span>
							</div>
							<div class="input-group field-desc" data-number="2">
								<input type="text" name="description[]" id="desk-f-2" class="form-control" required>
								<span class="input-group-btn">
									<button class="btn btn-danger del-field" type="button">Удалить</button>
								</span>
							</div>
							<span class="btn btn-primary add-field-desc">Добавить пункт</span>
						</div>
					</fieldset>

					
					<div class="panel-group" id="accordion">
					@foreach ($siteForms as $form)
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
				              <a data-toggle="collapse" data-parent="#accordion" href="#form-tab-{{ $form->id }}">
				                <input type="radio" name="id_form[]" value="{{ $form->id }}" id="form-{{ $form->id }}-f" {{ ($form->id != 2) ? 'checked' : '' }}>
								<label for="form-{{ $form->id }}-f">{{ $form->title }}</label>
				              </a>
				          </h4>
					    </div>
					    <div id="form-tab-{{ $form->id }}" class="panel-collapse collapse {{ ($form->id != 2) ? 'in' : ''}}">
					      <div class="panel-body">
					        <table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Название и описание</th>
										<th>Добавить</th>
										<th>По умолчанию</th>
										<th>Заблокировать</th>
									</tr>
								</thead>
								<tbody>

									@foreach($form->sectionConfig as $config)
									<tr>
										<td>{{ $config->section->title }}<hr>{{ $config->section->description }}</td>
										<td><input type="checkbox" name="sections_all_{{ $form->id }}[]" value="{{ $config->id }}"></td>
										<td><input type="checkbox" name="sections_default_{{ $form->id }}[]" value="{{ $config->id }}" disabled></td>
										<td><input type="checkbox" name="sections_block_{{ $form->id }}[]" value="{{ $config->id }}" disabled></td>
									</tr>
									@endforeach
									
								</tbody>
							</table>
					      </div>
					    </div>
					  </div>
					  @endforeach
					</div>
					

					

					<button type="submit" class="btn btn-primary">Сохранить</button>
					
				</form>
			</div>
		</div>

@endsection