@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Тип - {{ $config->siteType->title }} ({{ $config->form->title }})</div>
			
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<form method="POST" action="{{ action('Constructor\SiteTypesController@update', ['config' => $config->id]) }}" role="form" class="form">

					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class="form-group">
					    <label for="title-f">Название</label>
					    <input type="text" name="title" id="title-f" value="{{ $config->siteType->title }}" class="form-control">
					</div>

					<fieldset class="panel panel-default">
						<div class="panel-heading">
							Подходит, если вы:
						</div>
						<div class="panel-body">
							@for($i = 0, $size = sizeof($config->description); $i < $size; $i++)
							<div class="input-group field-desc" data-number="{{ $i + 1 }}">
								<input type="text" name="description[]" id="desk-f-{{ $i + 1 }}" value="{{ $config->description[$i] }}" class="form-control"  class="form-control">
								<span class="input-group-btn">
									<button class="btn btn-danger del-field" type="button">Удалить</button>
								</span>
							</div>
							@endfor
							<span class="btn btn-primary add-field-desc">Добавить пункт</span>
						</div>
					</fieldset>

					<input type="hidden" name="id_form" value="{{ $form->id }}" id="id-form">

					<fieldset class="panel panel-default">
						
						<div class="panel-heading">
							Разделы
						</div>
						<div class="panel-body">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Название и описание</th>
										<th>Добавить</th>
										<th>По умолчанию</th>
										<th>Заблокировать</th>
									</tr>
								</thead>
								<tbody>

									@foreach($form->sectionConfig as $configSection)
									<tr>
										<td>{{ $configSection->section->title }}<hr>{{ $configSection->section->description }}</td>
										<td><input type="checkbox" name="sections_all[]" value="{{ $configSection->id }}" 
										{{ in_array($configSection->id, $config->id_sections) ? 'checked' : ''}}
										></td>
										<td><input type="checkbox" name="sections_default[]" value="{{ $configSection->id }}" 
										{{ in_array($configSection->id, $config->id_sections_default) ? 'checked' : ''}}
										{{ !in_array($configSection->id, $config->id_sections) ? 'disabled' : ''}}
										></td>
										<td><input type="checkbox" name="sections_block[]" value="{{ $configSection->id }}" 
										{{ in_array($configSection->id, $config->id_sections_block) ? 'checked' : ''}}
										{{ !in_array($configSection->id, $config->id_sections) ? 'disabled' : ''}}
										></td>
									</tr>
									
									@endforeach
									
								</tbody>
							</table>
						</div>
					</fieldset>

					<button type="submit" class="btn btn-primary">Сохранить</button>
					
				</form>
			</div>
		</div>

@endsection