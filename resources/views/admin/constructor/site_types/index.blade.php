@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Типы сайтов</div>
			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>

				<div class="well well-sm">
				<a href="{{ action('Constructor\SiteTypesController@create') }}" class="btn btn-primary">Создать новый тип</a>
				</div>
				<table class="table table-bordered table-hover items">
					<thead>
						<tr>
							<th>Название</th>
							<th>Тип</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach($types as $type)
						<tr>
							<td rowspan="{{$type->forms->count() + 1}}">{{ $type->title }}</td>
							<td></td>
							<td></td>
						</tr>


						@foreach ($type->forms as $form)
						<tr>
							<td>{{ $form->title }}</td>
							<td>
								<a href="{{ action('Constructor\SiteTypesController@edit', ['type'=>$form->pivot->id]) }}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
								
								<a href="{{ action('Constructor\SiteTypesController@destroy', ['type'=>$form->pivot->id]) }}" data-token="{{ csrf_token() }}" class="btn btn-danger del"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
						</tr>
						@endforeach

						@endforeach
					</tbody>
				</table>
			</div>
		</div>

@endsection