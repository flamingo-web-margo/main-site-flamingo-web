@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Страницы (разделы)</div>
			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>

				<div class="well well-sm">
				<a href="{{ action('Constructor\SectionsController@create') }}" class="btn btn-primary">Создать новый раздел</a>
				</div>

				<table class="table table-bordered items">
					<thead>
						<tr>
							<th>Название и описание</th>
							<th>Вид сайта</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>

						@foreach($sections as $section)

						<tr  class="name">
							<td rowspan="{{$section->forms->count() + 1}}">{{ $section->title }}<hr>{{ $section->description }}</td>
							<td></td>
							<td></td>
						</tr>

						@foreach ($section->forms as $form)
						<tr>
							<td>{{ $form->title }}</td>
							<td>
								<a href="{{ action('Constructor\SectionsController@edit', ['section'=>$form->pivot->id]) }}" class="btn btn-primary "><span class="glyphicon glyphicon-pencil"></span></a>
								
								<a href="{{ action('Constructor\SectionsController@destroy', ['section'=>$form->pivot->id]) }}" data-token="{{ csrf_token() }}" class="btn btn-danger del"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
						</tr>
						@endforeach

						@endforeach
					</tbody>
				</table>

			</div>
		</div>
@endsection