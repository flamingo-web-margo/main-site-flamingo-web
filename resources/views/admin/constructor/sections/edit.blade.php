@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">
				Раздел - {{ $config->section->title }} ({{ $config->siteForm->title }})
			</div>
			
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<form method="POST" action="{{ action('Constructor\SectionsController@update', ['config' => $config->id]) }}" role="form" class="form">

					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<div class="form-group">
					    <label for="title-f">Название</label>
					    <input type="text" name="title" id="title-f" value="{{ $config->section->title }}" class="form-control">
					</div>

					<div class="form-group">
					    <label for="desc-f"></label>
					    <textarea name="description" id="desc-f" class="form-control">{{ $config->section->description }}</textarea>
					</div>

					<input type="hidden" name="id_form" value="{{ $config->siteForm->id }}" id="id-form">

					<fieldset class="panel panel-default">
						<div class="panel-heading">
							Элементы
						</div>
						<div class="panel-body">

						<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Название и описание</th>
										<th>Добавить</th>
										<th>По умолчанию</th>
										<th>Заблокировать</th>
									</tr>
								</thead>
								<tbody>	
									@foreach($form->elementConfig as $elemConfig)
									<tr>
										<td>{{ $elemConfig->element->title }}<hr>{{ $elemConfig->element->description }}</td>
										<td><input type="checkbox" name="elems_all[]" value="{{ $elemConfig->id }}" 
										{{ in_array($elemConfig->id, $config->id_elements) ? 'checked' : '' }}></td>

										<td><input type="checkbox" name="elems_default[]" value="{{ $elemConfig->id }}"
										{{ in_array($elemConfig->id, $config->id_elements_default) ? 'checked' : '' }}
										{{ !in_array($elemConfig->id, $config->id_elements) ? 'disabled' : '' }}
										></td>

										<td><input type="checkbox" name="elems_block[]" value="{{ $elemConfig->id }}"
										{{ in_array($elemConfig->id, $config->id_elements_block) ? 'checked' : '' }}
										{{ !in_array($elemConfig->id, $config->id_elements) ? 'disabled' : '' }}
										></td>
									</tr>
									@endforeach

								</tbody>
							</table>

							<div class="form-group">
								<label for="sort-f">Сортировка</label>
								<input type="number" min="0" name="sort" id="sort-f" class="form-control" value="{{ $config->sort }}">
							</div>
						</div>
					</fieldset>					

					<button type="submit" class="btn btn-primary">Сохранить</button>
					
				</form>
			</div>
		</div>

@endsection