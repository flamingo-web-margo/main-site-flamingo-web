@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Создание раздела</div>
			
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<form method="POST" action="{{ action('Constructor\SectionsController@store') }}" role="form" class="form">

					{{ csrf_field() }}

					<div class="form-group">
					    <label for="title-f">Название</label>
					    <input type="text" name="title" id="title-f" class="form-control">
					</div>

					<div class="form-group">
					    <label for="desc-f">Описание</label>
					    <textarea name="description" id="desc-f" class="form-control"></textarea>
					</div>

					@foreach ($siteForms as $form)
					<fieldset class="panel panel-default">
						
						<div class="panel-heading">
							<label for="form-{{ $form->id }}-f">{{ $form->title }}</label>
							<input type="checkbox" name="id_form[]" value="{{ $form->id }}" id="form-{{ $form->id }}-f" {{ ($form->id == 2) ? 'checked' : '' }}>
						</div>
						<div class="panel-body">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Название и описание</th>
										<th>Добавить</th>
										<th>По умолчанию</th>
										<th>Заблокировать</th>
									</tr>
								</thead>
								<tbody>
								
									@foreach($form->elementConfig as $config)
									<tr>
										<td>{{ $config->element->title }}<hr>{{ $config->element->description }}</td>
										<td><input type="checkbox" name="elems_all_{{ $form->id }}[]" value="{{ $config->id }}"></td>
										<td><input type="checkbox" name="elems_default_{{ $form->id }}[]" value="{{ $config->id }}" disabled></td>
										<td><input type="checkbox" name="elems_block_{{ $form->id }}[]" value="{{ $config->id }}" disabled></td>
									</tr>
									@endforeach
									
								</tbody>
							</table>
							<div class="form-group">
								<label for="sort-f-{{ $form->id }}">Сортировка</label>
								<input type="number" min="0" name="sort-{{ $form->id }}-form" id="sort-f-{{ $form->id }}"  class="form-control" value="{{ old('sort-{ $form->id }-form') }}">
							</div>
						</div>
					</fieldset>
					@endforeach			

					<button type="submit" class="btn btn-primary">Сохранить</button>
					
				</form>
			</div>
		</div>

@endsection