@extends('admin.layouts.app')

@section('content')


    	<h2 class="text-center">Заказы</h2><br>

		<div class="panel panel-default">
			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>

				<table class="table table-bordered table-hover">
				    <thead>
					    <tr>
					    	<td>Поступил</td>
					    	<td>Название</td>
							<td>Сроки</td>
							<td>Клиент</td>
							<td>Стоимость</td>
							<td>Выполнено</td>
							<td>Действия</td>
						</tr>
				    </thead>
				    <tbody>
					    @foreach ($orders as $order)
						<tr>
							<td>{{ $order->created_at->format('Y.m.d H:i') }}</td>
							<td>
								<span class="label label-{{ $order->type->id == 1 ? 'info' : 'warning' }}">{{ $order->type->title }}</span>
								заказ на сайт типа 
								"{{ $order->config->site->siteType->title }}"


							</td>
							<td>
							{{ $order->date_start ? $order->date_start->format('Y.m.d') : 'не определено'}} - 
							{{ $order->date_end ? $order->date_end->format('Y.m.d') : 'не определено'}}</td>
							<td>
								<a href="{{ action('ClientsController@show', ['client' => $order->client->id]) }}">
									{{ $order->client->name }}
								</a>
							</td>
							<td>{{ $order->price }}</td>
							<td>{{ $order->performed }}%</td>
							<td>
								<a href="{{ action('OrdersController@show', ['order'=>$order->id]) }}"  class="btn btn-primary">
									<span class="glyphicon glyphicon-eye-open"></span>
								</a>
								<a href="{{ action('OrdersController@edit', ['order'=>$order->id]) }}"  class="btn btn-primary">
									<span class="glyphicon glyphicon-pencil"></span>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>

@endsection