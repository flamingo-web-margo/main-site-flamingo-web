@extends('admin.layouts.app')

@section('content')



		<h2 class="text-center">{{ $order->type->title }} заказ на сайт типа "{{ $order->config->site->siteType->title }}"</h2><br>

		<div class="panel panel-default">
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">Данные заказа</div>

							<div class="panel-body">
								<table class="table table-bordered table-hover">
									<tbody>
										<tr>
											<td>Поступил</td>
											<td>{{ $order->created_at->format('d.m.Y') }}</td>
										</tr>
										<tr>
											<td>Сроки выполнения:</td>
											<td>{{ $order->date_start ? $order->date_start->format('d.m.Y') : 'не определено'}} - 
											{{ $order->date_end ? $order->date_end->format('d.m.Y') : 'не определено' }}</td>
										</tr>
										<tr>
											<td>Сумма</td>
											<td>{{ $order->price }}</td>
										</tr>
										<tr>
											<td>Выполнено</td>
											<td>{{ $order->performed }}%</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">Данные клиента</div>

							<div class="panel-body">
								<table class="table table-bordered table-hover">
									<tbody>
										<tr>
											<td>Имя</td>
											<td>
												<a href="{{ action('ClientsController@show', ['client' => $order->client->id]) }}">
													{{ $order->client->name }}
												</a>
											</td>
										</tr>
										<tr>
											<td>Телефон</td>
											<td>{{ $order->client->tel }}</td>
										</tr>
										<tr>
											<td>E-mail</td>
											<td>{{ $order->client->email }}</td>
										</tr>
										<tr>
											<td>Дата Рождения</td>
											<td>{{ $order->client->dob }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">Сайт типа {{ $order->config->site->siteType->title }} с разделами:</div>
							<div class="panel-group" id="accordion">

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#section-0"  class="btn btn-link">
											Основные элементы
											</a>
										</h4>
									</div>
									<div id="section-0" class="panel-collapse collapse">
										<div class="panel-body">
											<table class="table table-bordered table-hover">
												<tbody>
													<tr>
														<td>{{ $order->config->detail_config->header->element->title }}</td>
														<td>{{ $order->config->detail_config->header->element->description }}</td>
														<td>{{ $order->config->detail_config->header->price }}руб</td>
													</tr>
													<tr>
														<td>{{ $order->config->detail_config->footer->element->title }}</td>
														<td>{{ $order->config->detail_config->footer->element->description }}</td>
														<td>{{ $order->config->detail_config->footer->price }}руб</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>

							@foreach($order->config->detail_config as $sectionConfig)
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#section-{{ $sectionConfig->id }}" class="btn btn-link">
												{{ $sectionConfig->section->title }} {{ $sectionConfig->price }}руб
											</a>
										</h4>
									</div>
									<div id="section-{{ $sectionConfig->id }}" class="panel-collapse collapse">
										<div class="panel-body">
											<table class="table table-bordered table-hover">
												<tbody>
												@foreach($sectionConfig->id_elements as $elementConfig)
													<tr>
														<td>{{ $elementConfig->element->title }}</td>
														<td>{{ $elementConfig->element->description }}</td>
														<td>{{ $elementConfig->price }}</td>
													</tr>
												@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							@endforeach
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">Дополнительные сведения</div>

							<div class="panel-body">
								<table class="table table-bordered table-hover">
									<tbody>
										<tr>
											<td>Описание</td>
											<td>{{ $order->config->description }}</td>
										</tr>
										<tr>
											<td>Понравившиеся сайты</td>
											<td>{{ $order->config->sites_like }}</td>
										</tr>
										<tr>
											<td>Логотип</td>
											<td>
												@if ($order->config->logotype != 'null')
												<a href='{{ url("public/{$order->config->logotype}") }}' target="_blank">
													<img src='{{ url("public/{$order->config->logotype}") }}' alt="Логотип заказа" width="150">
												</a>
												@endif
											</td>
										</tr>
										<tr>
											<td>Адаптивность</td>
											<td>{{ $order->config->adaptive < 1 ? 'нет' : 'да'}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">{{ ($order->config->site->id_form == 2) ? 'Макеты' : 'Макет' }}</div>

							@if ($order->config->site->id_form == 2) 
								<div class="panel-group" id="accordion-2">
								@foreach($order->config->detail_config as $sectionConfig)
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion-2" href="#section-{{ $sectionConfig->id }}-mockup">
													{{ $sectionConfig->section->title }}
												</a>
											</h4>
										</div>
										<div id="section-{{ $sectionConfig->id }}-mockup" class="panel-collapse collapse">
											<div class="panel-body">
												<figure>
													<ul  class="list-unstyled">
														<li>
															<img src='{{ url("public/{$order->config->detail_config->header->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
														</li><br>
														@foreach ($sectionConfig->id_elements as $elementConfig)
														<li>
															<img src='{{ url("public/{$elementConfig->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
														</li><br>
														@endforeach
														<li>
															<img src='{{ url("public/{$order->config->detail_config->footer->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
														</li>
													</ul>
												</figure>
											</div>
										</div>
									</div>
								@endforeach
								</div>
							@else
								<figure>
									<ul  class="list-unstyled">
										<li>
											<img src='{{ url("public/{$order->config->detail_config->header->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
										</li><br>
										<li>
											<ol  class="list-unstyled">
												@foreach($order->config->detail_config as $sectionConfig)
													<li>
														<ul  class="list-unstyled">
															@foreach ($sectionConfig->id_elements as $elementConfig)
															<li>
																<img src='{{ url("public/{$elementConfig->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
															</li><br>
															@endforeach
														</ul>
													</li>
												@endforeach
											</ol>
										</li>
										<li>
											<img src='{{ url("public/{$order->config->detail_config->footer->img}") }}' alt="Элемент - {{ $elementConfig->element->title }}">
										</li>
									</ul>

								</figure>
							@endif
						</div>
					</div>
				</div>

			</div>
		</div>

@endsection