@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">{{ $order->type->title }} заказ на сайт типа "{{ $order->config->site->siteType->title }}"</div>
			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<form method="POST" action="{{ action('OrdersController@update', ['order' => $order->id]) }}" enctype="multipart/form-data" role="form">

					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset class="panel panel-default">
						<div class="panel-heading">Сроки</div>
						<div class="panel-body">
							<div class="form-group">
								<label for="date-start-f">Дата старта</label>
							   	<input type="date" name="date_start" id="date-start-f" value="{{ $order->date_start ? $order->date_start->format('Y-m-d') : $order->created_at->format('Y-m-d')}}" class="form-control" >
							    
								@if ($errors->has('date_start'))
							    <span class="text-danger">{{ $errors->first('date_start') }}</span>
							    @endif
							</div>
							<div class="form-group">
								<label for="date-end-f">Дата завершения</label>
							   	<input type="date" name="date_end" id="date-end-f" value="{{ $order->date_end ? $order->date_end->format('Y-m-d') : $order->created_at->format('Y-m-d')}}" class="form-control" >
							    
								@if ($errors->has('date_end'))
							    <span class="text-danger">{{ $errors->first('date_end') }}</span>
							    @endif
							</div>
						</div>
					</fieldset>


					<fieldset class="panel panel-default" id="performed-panel">
						<div class="panel-heading">Выполнение</div>
						<div class="panel-body">

							<div class="progress">
							  <div class="progress-bar" role="progressbar" aria-valuenow="{{ $order->performed }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $order->performed }}%;">
							    {{ $order->performed }}%
							  </div>
							</div>

							<div class="radio">
								<label>
								    <input type="radio" name="performed" value="0" {{ $order->performed == 0 ? 'checked' : ''}}>
								      Начало
								</label>
							</div>

							<div class="radio">
								<label>
								    <input type="radio" name="performed" value="10" {{ $order->performed == 10 ? 'checked' : ''}}>
								      Техническое задание утверждено
								</label>
							</div>

							<div class="radio">
								<label>
								    <input type="radio" name="performed" value="20" {{ $order->performed == 20 ? 'checked' : ''}}>
								      Необходимые материалы собраны
								</label>
							</div>

							<div class="radio">
								<label>
								    <input type="radio" name="performed" value="50" {{ $order->performed == 60 ? 'checked' : ''}}>
								      Клиентская часть готова
								</label>
							</div>

							<div class="radio">
								<label>
								    <input type="radio" name="performed" value="99" {{ $order->performed == 99 ? 'checked' : ''}}>
								      Серверная часть готова
								</label>
							</div>

							<div class="radio">
								<label>
								    <input type="radio" name="performed" value="100" {{ $order->performed == 100 ? 'checked' : ''}}>
								      Завершён
								</label>
							</div>
							<br>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">Сохранить</button>

				</form>

			</div>
		</div>

@endsection