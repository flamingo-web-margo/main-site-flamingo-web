@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Сообщения пользователя {{ $message->name }}</div>

			<div class="panel-body">

				<div id="alerts">
					@if (session('message'))
						<div class="alert alert-success">{{ session('message') }}</div>
					@endif
					@if (session('error'))
						<div class="alert alert-danger">{{ session('error') }}</div>
					@endif
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">Контактные данные</div>

					<div class="panel-body">
						<table class="table table-bordered table-hover">

							<thead>
								<tr>
									<th>Имя</th>
									<th>E-mail</th>
									<th>Действия</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ $message->name }}</td>
									<td>{{ $message->email }}</td>
									<td>
										@if ($message->tel)
											<a href="tel:{{ $message->tel }}" class="btn btn-primary"><span class="glyphicon glyphicon-earphone"></span></a>
										@endif

										@if ($message->email)
											 <a href="mailto:{{ $message->email }}" class="btn btn-primary"><span class="glyphicon glyphicon-envelope"></span></a>
										@endif
									   
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>


				<div class="panel panel-default">
					<div class="panel-heading">Сообщения</div>

					<div class="panel-body">
						<table class="table table-bordered table-hover">

							<thead>
								<tr>
									<th>Дата</th>
									<th>Сообщение</th>
								</tr>
							</thead>
							<tbody>
								@foreach($messages as $message)
								<tr>
									<td>{{ $message->created_at }}</td>
									<td>{{ $message->text }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
@endsection