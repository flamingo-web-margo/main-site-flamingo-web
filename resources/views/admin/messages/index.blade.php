@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Сообщения</div>

			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Дата</th>
					        <th>Имя</th>
					        <th>E-mail</th>
					        <th>Сообщение</th>
					        <th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach($messages as $message)
					    <tr>
					    	<td>{{ $message->created_at->format('Y.m.d H:i') }}</td>
					        <td>{{ $message->name }}</td>
					        <td>{{ $message->email }}</td>
					        <td>{{ str_limit($message->text, 150) }}</td>
					        <td>
					            <a href="{{ action('MessagesController@show', ['message'=>$message->id]) }}" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span></a>
					        </td>
					    </tr>
					    @endforeach
					</tbody>
				</table>
			</div>
		</div>
@endsection