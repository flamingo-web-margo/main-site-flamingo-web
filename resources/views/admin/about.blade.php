@extends('admin.layouts.app')

@section('content')
	<div class="panel panel-primary">
		<div class="panel-heading">Данные компании</div>
		<div class="panel-body">

			<div id="alerts">
				@if (session('message'))
					<div class="alert alert-success">{{ session('message') }}</div>
				@endif
				@if (session('error'))
					<div class="alert alert-danger">{{ session('error') }}</div>
				@endif
			</div>

			<form method="POST" action="{{ route('adminAboutUpdate') }}" role="form">
				{{ csrf_field() }}

				<div class="form-group">
				    <label for="description-f">Описание</label>
				    <textarea name="description" id="description-f" class="form-control" rows="3">{{ Company::info()->description }}</textarea>
				</div>

				<fieldset class="panel panel-default">
					<div class="panel-heading">
						Контактные данные
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label for="tel-f">Телефон</label>
							<input type="tel" name="tel" value="{{ Company::info()->tel }}" id="tel-f" class="form-control">
						</div>
						<div class="form-group">
							<label for="email-f">E-mail</label>
							<input type="mail" name="email" value="{{ Company::info()->email }}" id="email-f" class="form-control">
						</div>
						<div class="form-group">
							<label for="vk-f">Вконтакте</label>
							<input type="text" name="vk" value="{{ Company::social()['vk'] }}" id="vk-f" class="form-control">
						</div>
						<div class="form-group">
							<label for="insta-f">Instagram</label>
							<input type="text" name="insta" value="{{ Company::social()['insta'] }}" id="insta-f" class="form-control">
						</div>
					</div>
				</fieldset>

				<div class="form-group">
					<label for="copyright-f">Copyright</label>
					<input type="text" name="copyright" value="{{ Company::info()->copyright }}" id="copyright-f" class="form-control">
				</div>

				<button type="submit" class="btn btn-primary">Сохранить</button>
				
			</form>
		</div>
	</div>
@endsection