@extends('admin.layouts.app')

@section('content')

    	<h2 class="text-center">Клиент - {{ $client->name }}</h2>

		<div class="panel panel-default">
			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>

				<table class="table table-bordered table-hover">
					<caption>Личные данные</caption>
					<tbody>
						<tr>
					        <td>Телефон</td>
					        <td>{{ $client->tel }}</td>
					    </tr>
					    <tr>
					        <td>E-mail</td>
					        <td>{{ $client->email }}</td>
					    </tr>
					    <tr>
					        <td>Дата Рождения</td>
					        <td>{{ $client->dob }}</td>
					    </tr>
					    <tr>
					        <td>Всего заказов</td>
					        <td>{{ $client->orders->count() }}</td>
					    </tr>
					    <tr>
					        <td>Заказов выполняется</td>
					        <td>{{ $client->orders->where('performed', '<', 100)->count() }}</td>
					    </tr>
					</tbody>
				</table>

				<table class="table table-bordered table-hover">
				    <caption>Заказы</caption>
				    <thead>
					    <tr>
					        <td>Дата поступления</td>
					        <td>Дата начала выполнения</td>
					        <td>Дата завершения выполнения</td>
					        <td>Выполнено</td>
					        <td>Действия</td>
					    </tr>
				    </thead>
				    <tbody>
					    @foreach ($client->orders as $order)
					    <tr>
					        <td>{{ $order->created_at->format('Y.m.d H:i') }}</td>
					        <td>{{ $order->date_start or 'не определено'}}</td>
					        <td>{{ $order->date_end or 'не определено'}}</td>
					        <td>{{ $order->performed }}%</td>
					        <td><a href="{{ action('OrdersController@show', ['order'=>$order->id]) }}" class="btn btn-primary">Просмотреть</a></td>
					    </tr>
					    @endforeach
					</tbody>
				</table>
			</div>
		</div>
@endsection