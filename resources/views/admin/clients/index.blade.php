@extends('admin.layouts.app')

@section('content')

		<div class="panel panel-primary">
			<div class="panel-heading">Клиенты</div>

			<div class="panel-body">

				<div id="alerts">
				    @if (session('message'))
				        <div class="alert alert-success">{{ session('message') }}</div>
				    @endif
				    @if (session('error'))
				        <div class="alert alert-danger">{{ session('error') }}</div>
				    @endif
				</div>

				<table class="table table-bordered table-hover">
					<thead>
						<tr>
					        <th>ФИО</th>
					        <th>Телефон</th>
					        <th>E-mail</th>
					        <th>Заказов выполняется</th>
					        <th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach($clients as $client)
					    <tr>
					        <td>{{ $client->name }}</td>
					        <td>{{ $client->tel }}</td>
					        <td>{{ $client->email }}</td>
					        <td>{{ $client->orders->where('performed', '<', 100)->count() }}</td>
					        <td>
					            <a href="{{ action('ClientsController@show', ['client'=>$client->id]) }}" class="btn btn-primary">Подробнее</a>
					        </td>
					    </tr>
					    @endforeach
					</tbody>
				</table>
			</div>
		</div>
@endsection