-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 04 2017 г., 15:36
-- Версия сервера: 10.1.21-MariaDB
-- Версия PHP: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `flamingo-web`
--

-- --------------------------------------------------------

--
-- Структура таблицы `clients_data`
--

CREATE TABLE `clients_data` (
  `id` int(11) NOT NULL,
  `tel` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `dob` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `clients_data`
--

INSERT INTO `clients_data` (`id`, `tel`, `dob`) VALUES
(1, '3455234342', '2017-06-15'),
(2, '3465727347467', '2017-06-15');

-- --------------------------------------------------------

--
-- Структура таблицы `company_data`
--

CREATE TABLE `company_data` (
  `id` int(11) NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `tel` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `copyright` varchar(255) COLLATE utf8_bin NOT NULL,
  `social_links` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `company_data`
--

INSERT INTO `company_data` (`id`, `description`, `tel`, `email`, `copyright`, `social_links`, `created_at`, `updated_at`) VALUES
(1, 'Наша небольшая компания находится в Ростове-на-Дону, но готова к сотрудничеству с клиентами по всей Южной России.<br>\r\nНаша идея заключается в том, чтобы помочь малому бизнесу за короткие сроки и небольшие деньги найти новых клиентов с помощью собственного сайта.', '+7 (918) 514-50-94', 'flamingo@gmail.com', 'Flamingo-web © 2018 | Ростов-на-Дону', '{\"vk\":\"https:\\/\\/vk.com\\/\",\"insta\":\"https:\\/\\/www.instagram.com\\/\"}', NULL, '2017-05-30 10:27:18');

-- --------------------------------------------------------

--
-- Структура таблицы `constructor_elements`
--

CREATE TABLE `constructor_elements` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `price` varchar(255) COLLATE utf8_bin NOT NULL,
  `img` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `constructor_elements`
--

INSERT INTO `constructor_elements` (`id`, `title`, `price`, `img`, `description`, `created_at`, `updated_at`) VALUES
(3, 'Небольшой текст-описание', '200', NULL, 'Описание', '2017-05-31 02:51:32', '2017-05-31 02:51:32'),
(4, 'Баннер', '1000', NULL, 'Описание', '2017-05-31 02:51:42', '2017-05-31 02:51:42'),
(5, 'Преимущества', '500', NULL, 'Описание', '2017-05-31 02:51:52', '2017-06-04 09:21:43'),
(6, 'Логотипы партнёров', '500', NULL, 'Описание', '2017-05-31 02:52:04', '2017-06-04 09:20:56'),
(7, 'Достижения', '500', NULL, 'Описание', '2017-05-31 02:52:22', '2017-05-31 02:52:22'),
(8, 'Команда', '1500', NULL, 'Описание', '2017-05-31 02:52:31', '2017-05-31 02:52:31'),
(13, 'Галерея', '3000', NULL, 'Описание', '2017-05-31 02:54:15', '2017-05-31 02:54:15'),
(14, 'Фильтрация в галереи', '500', NULL, 'Описание', '2017-05-31 02:54:37', '2017-06-04 08:50:31'),
(15, 'Портфолио', '3000', NULL, 'Описание', '2017-05-31 02:54:50', '2017-05-31 02:54:50'),
(16, 'Сортировка в портфолио', '500', NULL, 'Описание', '2017-05-31 02:55:03', '2017-05-31 02:55:03'),
(17, 'Фильтрация в портфолио', '500', NULL, 'Описание', '2017-05-31 02:55:18', '2017-05-31 02:55:18'),
(18, 'Отзывы', '3000', NULL, 'Описание', '2017-05-31 02:55:43', '2017-06-04 09:21:28'),
(19, 'Новости', '3000', NULL, 'Описание', '2017-05-31 02:56:13', '2017-06-04 09:21:08'),
(20, 'Сортировка в новостях', '500', NULL, 'Описание', '2017-05-31 02:56:56', '2017-05-31 02:56:56'),
(21, 'Фильтрация в новостях', '500', NULL, 'Описание', '2017-05-31 02:57:07', '2017-05-31 02:57:07'),
(22, 'Форма обратной связи', '1000', NULL, 'Описание', '2017-05-31 02:57:26', '2017-05-31 02:57:26'),
(24, 'Форма \"Заказать звонок\"', '500', NULL, 'Описание', '2017-05-31 02:58:11', '2017-05-31 02:58:11'),
(25, 'Область с контактными данными', '300', NULL, 'Описание', '2017-05-31 02:58:36', '2017-05-31 02:58:36'),
(26, 'Карта', '1000', NULL, 'Описание', '2017-05-31 02:58:42', '2017-05-31 02:58:42'),
(28, 'Текст О компании / Обо мне', '500', NULL, 'Описание', '2017-05-31 02:59:47', '2017-05-31 02:59:47'),
(29, 'Услуги', '3000', NULL, 'Описание', '2017-05-31 03:00:05', '2017-06-04 08:47:50'),
(30, 'Сортировка в услугах', '500', NULL, 'Описание', '2017-05-31 03:00:24', '2017-05-31 03:00:24'),
(31, 'Фильтрация в услугах', '500', NULL, 'Описание', '2017-05-31 03:00:34', '2017-05-31 03:00:34'),
(32, 'Блог', '3000', NULL, 'Описание', '2017-05-31 03:59:05', '2017-05-31 03:59:05'),
(33, 'Фильтрация в блоге', '500', NULL, 'Описание', '2017-05-31 03:59:21', '2017-05-31 03:59:21'),
(34, 'Сортировка в блоге', '500', NULL, 'Описание', '2017-05-31 03:59:38', '2017-05-31 03:59:38'),
(35, 'Акции', '3000', NULL, 'Описание', '2017-05-31 03:59:47', '2017-06-03 18:39:07'),
(36, 'Фильтрация в акциях', '500', NULL, 'Описание', '2017-05-31 04:00:07', '2017-06-04 09:22:04'),
(37, 'Сортировка в акциях', '500', NULL, 'Описание', '2017-05-31 04:00:19', '2017-05-31 04:00:19'),
(38, 'Сортировка в галереи', '500', NULL, 'Описание', '2017-05-31 04:00:55', '2017-05-31 04:00:55');

-- --------------------------------------------------------

--
-- Структура таблицы `constructor_sections`
--

CREATE TABLE `constructor_sections` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` varchar(255) COLLATE utf8_bin NOT NULL,
  `price` varchar(255) COLLATE utf8_bin NOT NULL,
  `elems` varchar(255) COLLATE utf8_bin NOT NULL,
  `elems_default` varchar(255) COLLATE utf8_bin DEFAULT '[]',
  `elems_block` varchar(255) COLLATE utf8_bin DEFAULT '[]',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `constructor_sections`
--

INSERT INTO `constructor_sections` (`id`, `title`, `description`, `price`, `elems`, `elems_default`, `elems_block`, `created_at`, `updated_at`) VALUES
(2, 'Главная', 'Этот раздел нельзя убрать', '1200', '[\"4\",\"7\",\"26\",\"8\",\"6\",\"3\",\"25\",\"5\",\"28\",\"24\",\"22\"]', '[\"4\",\"3\"]', '[]', '2017-05-31 03:04:03', '2017-06-04 09:22:23'),
(3, 'О нас / обо мне', 'Описание', '500', '[\"7\",\"6\",\"25\",\"18\",\"5\",\"28\",\"22\"]', '[\"28\"]', '[\"28\"]', '2017-05-31 03:07:01', '2017-06-04 09:22:55'),
(4, 'Новости', 'Описание', '3000', '[\"19\",\"20\",\"21\"]', '[\"19\"]', '[\"19\"]', '2017-05-31 03:08:03', '2017-06-04 09:23:13'),
(5, 'Услуги', 'Описание', '3000', '[\"30\",\"29\",\"31\"]', '[\"29\"]', '[\"29\"]', '2017-05-31 03:09:32', '2017-06-04 09:23:44'),
(6, 'Портфолио', 'Описание', '3000', '[\"15\",\"16\",\"17\"]', '[\"15\"]', '[\"15\"]', '2017-05-31 03:11:18', '2017-06-04 09:23:31'),
(7, 'Галерея', 'Описание', '3000', '[\"13\",\"38\",\"14\"]', '[\"13\"]', '[\"13\"]', '2017-05-31 03:11:40', '2017-06-04 09:24:03'),
(8, 'Отзывы', 'Описание', '3000', '[\"18\"]', '[\"18\"]', '[\"18\"]', '2017-05-31 03:12:27', '2017-06-04 09:24:18'),
(9, 'Команда', 'Описание', '1500', '[\"8\"]', '[\"8\"]', '[\"8\"]', '2017-05-31 03:52:46', '2017-06-04 09:24:32'),
(10, 'Контакты', 'Описание', '300', '[\"26\",\"25\",\"22\"]', '[\"25\"]', '[\"25\"]', '2017-05-31 03:55:23', '2017-06-04 09:24:47'),
(11, 'Блог', 'Описание', '3000', '[\"32\",\"34\",\"33\"]', '[\"32\"]', '[\"32\"]', '2017-05-31 04:04:45', '2017-06-04 09:24:58'),
(12, 'Акции', 'Описание', '3000', '[\"35\",\"37\",\"36\"]', '[\"35\"]', '[\"35\"]', '2017-05-31 04:05:13', '2017-06-04 09:13:17');

-- --------------------------------------------------------

--
-- Структура таблицы `constructor_steps`
--

CREATE TABLE `constructor_steps` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `desc_min` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `constructor_steps`
--

INSERT INTO `constructor_steps` (`id`, `number`, `title`, `desc_min`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Выбор типа заказа', 'Если вас что-либо не устраивает в обычном заказе, можете в любой момент перейти к индивидуальному', 'Если вас что-либо не устраивает в обычном заказе, можете в любой момент перейти к индивидуальному', NULL, '2017-06-03 13:59:42'),
(2, 2, 'Выбор типа сайта', 'Какой сайт вам нужен? Портфолио, визитка, Landing Page или что-то другое?', 'Какой сайт вам нужен? Портфолио, визитка, Landing Page или что-то другое?', NULL, '2017-05-29 08:03:21'),
(3, 3, 'Определение разделов', 'Решите какие разделы должны быть на вашем сайте и уберите те, которые не нужны', 'Решите какие разделы должны быть на вашем сайте и уберите те, которые не нужны', NULL, NULL),
(4, 4, 'Настройка элементов', 'Определитесь, какой функционал вам нужен и как должны выглядеть главные элементы', 'Определитесь, какой функционал вам нужен и как должны выглядеть главные элементы', NULL, NULL),
(5, 5, 'Дополнительная информация', 'Расскажите нам подробнее о себе или о своей компании', 'Расскажите нам подробнее о себе или о своей компании', NULL, NULL),
(6, 6, 'Оформление заказа', 'После оформления заказа вы сможете отслеживать статус его выполнения в своём личном кабинете', 'После оформления заказа вы сможете отслеживать статус его выполнения в своём личном кабинете', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `sites_like` text COLLATE utf8_bin,
  `logotype` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `adaptive` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `mockup` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `site_type` int(11) DEFAULT NULL,
  `sections` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `elements` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `performed` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `price` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `client_id`, `description`, `sites_like`, `logotype`, `adaptive`, `mockup`, `site_type`, `sections`, `elements`, `performed`, `price`, `date_start`, `date_end`, `created_at`, `updated_at`) VALUES
(1, 3, 'Описание', 'Примеры', 'orders-logo/2017-06-02/Flamingo-web.png', 'on', NULL, 1, '[\"2\",\"10\",\"7\",\"9\"]', '{\"2\":[\"3\",\"4\"],\"7\":[\"13\"],\"9\":[\"8\"],\"10\":[\"25\",\"22\",\"26\"]}', '70', '50000', '2017-06-01', NULL, '2017-06-02 04:30:29', '2017-06-02 04:30:29'),
(2, 3, 'Описание', 'Примеры', 'orders-logo/2017-06-02/309448Flamingo-web.png', 'on', NULL, 1, '[\"2\",\"10\"]', '{\"2\":[\"3\",\"4\"],\"7\":[\"13\"],\"9\":[\"8\"],\"10\":[\"25\",\"22\",\"26\"]}', '0', '50000', '2017-06-01', NULL, '2017-06-02 04:54:03', '2017-06-02 04:54:03'),
(3, 2, 'Описание', 'Примеры', 'orders-logo/2017-06-02/Flamingo-web.png', 'on', NULL, 1, '[\"2\",\"10\",\"7\",\"9\"]', '{\"2\":[\"3\",\"4\"],\"7\":[\"13\"],\"9\":[\"8\"],\"10\":[\"25\",\"22\",\"26\"]}', '80', '50000', '2017-06-01', NULL, '2017-06-02 05:45:12', '2017-06-02 05:45:12'),
(4, 2, 'Описание', 'Примеры', 'null', 'on', NULL, 1, '[\"2\",\"10\"]', '{\"2\":[\"3\",\"4\"],\"7\":[\"13\"],\"9\":[\"8\"],\"10\":[\"25\",\"22\",\"26\"]}', '70', '50000', '2017-06-01', NULL, '2017-06-02 06:08:40', '2017-06-02 06:08:40'),
(5, 2, 'Описание', 'Примеры', 'null', 'on', NULL, 1, '[\"2\",\"10\"]', '{\"2\":[\"3\",\"4\"],\"7\":[\"13\"],\"9\":[\"8\"],\"10\":[\"25\",\"22\",\"26\"]}', '100', '50000', '2017-06-01', '2017-06-02', '2017-06-02 06:10:43', '2017-06-02 06:10:43'),
(6, 2, 'Описание', 'Примеры', 'null', 'on', NULL, 1, '[\"2\",\"10\",\"7\",\"9\"]', '{\"2\":[\"3\",\"4\"],\"7\":[\"13\"],\"9\":[\"8\"],\"10\":[\"25\",\"22\",\"26\"]}', '0', '50000', '2017-06-01', NULL, '2017-06-02 06:25:38', '2017-06-02 06:25:38'),
(7, 2, 'Описание', 'Примеры', 'null', 'on', NULL, 1, '[\"2\",\"10\"]', '{\"2\":[\"3\",\"4\"],\"7\":[\"13\"],\"9\":[\"8\"],\"10\":[\"25\",\"22\",\"26\"]}', '0', '50000', NULL, NULL, '2017-06-02 06:47:14', '2017-06-02 06:47:14'),
(8, 3, 'Описание', 'Примеры', 'null', 'on', NULL, 1, '[\"2\",\"10\",\"7\"]', '{\"2\":[\"3\",\"4\"],\"7\":[\"13\"],\"9\":[\"8\"],\"10\":[\"25\",\"22\",\"26\"]}', '0', '50000', NULL, NULL, '2017-06-02 09:38:31', '2017-06-02 09:38:31'),
(9, 4, 'Описание', 'Примеры', 'null', 'on', NULL, 1, '[\"2\",\"3\",\"10\",\"5\"]', '{\"2\":[\"3\",\"4\"],\"3\":[\"28\",\"5\"],\"5\":[\"29\"],\"6\":[],\"7\":[],\"8\":[],\"10\":[\"25\"]}', '0', NULL, NULL, NULL, '2017-06-04 09:36:33', '2017-06-04 09:36:33'),
(10, 5, 'Описание', 'Примеры', 'null', 'on', NULL, 1, '[\"2\",\"3\",\"10\",\"5\"]', 'undefined', '0', NULL, NULL, NULL, '2017-06-04 10:14:09', '2017-06-04 10:14:09');

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `img` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_bin NOT NULL,
  `order_type` varchar(255) COLLATE utf8_bin NOT NULL,
  `site_type_id` int(11) NOT NULL,
  `created_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `title`, `description`, `img`, `link`, `order_type`, `site_type_id`, `created_date`, `created_at`, `updated_at`) VALUES
(2, 'Сайт компании INSITU', 'Красивый сайт для крупной компании находящейся в Израиле', 'uploads/2017-05-29/w-2.png', 'https://trello.com/b/qcbNavI3/flamingo-web', 'Индивидуальный', 1, '2017-05-29', '2017-05-29 06:20:07', '2017-06-03 12:54:11'),
(4, 'La Fruteria', 'Оооо, пора этот сайт наконец-таки переделать', 'uploads/2017-05-29/w-5.png', 'https://trello.com/b/qcbNavI3/flamingo-web', 'Обычный', 1, '2017-05-29', '2017-05-29 06:21:34', '2017-05-29 06:21:34');

-- --------------------------------------------------------

--
-- Структура таблицы `site_types`
--

CREATE TABLE `site_types` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `type` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT 'Многостраничный',
  `description` text COLLATE utf8_bin NOT NULL,
  `price` varchar(255) COLLATE utf8_bin NOT NULL,
  `sections` varchar(255) COLLATE utf8_bin DEFAULT '[]',
  `sections_default` varchar(255) COLLATE utf8_bin DEFAULT '[]',
  `elems` varchar(255) COLLATE utf8_bin DEFAULT '[]',
  `elems_default` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '[]',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `site_types`
--

INSERT INTO `site_types` (`id`, `title`, `type`, `description`, `price`, `sections`, `sections_default`, `elems`, `elems_default`, `created_at`, `updated_at`) VALUES
(1, 'Сайт-визитка', 'Многостраничный', '[\"\\u043e\\u043a\\u0430\\u0437\\u044b\\u0432\\u0430\\u0435\\u0442\\u0435 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438\",\"\\u0432\\u043b\\u0430\\u0434\\u0435\\u0435\\u0442\\u0435 \\u043d\\u0435\\u0431\\u043e\\u043b\\u044c\\u0448\\u043e\\u0439 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0435\\u0439\"]', '2000', '[\"7\",\"2\",\"10\",\"3\",\"8\",\"6\",\"5\"]', '[\"2\",\"10\",\"3\"]', '[\"3\"]', '[\"3\"]', '2017-05-30 15:36:19', '2017-06-04 09:25:53'),
(4, 'Блог', 'Многостраничный', '[\"1\",\"2\",\"3\"]', '5000', '[\"11\",\"2\",\"10\",\"3\"]', '[\"11\",\"2\",\"10\",\"3\"]', '[\"3\"]', '[\"3\"]', '2017-05-30 15:37:09', '2017-06-04 09:26:03'),
(5, 'Портфолио', 'Многостраничный', '[\"1\",\"2\",\"3\"]', '5000', '[\"11\",\"2\",\"10\",\"3\",\"6\"]', '[\"2\",\"10\",\"3\",\"6\"]', '[\"3\"]', '[\"3\"]', '2017-05-30 15:37:21', '2017-06-04 09:26:10'),
(6, 'Корпоративный сайт', 'Многостраничный', '[\"1\",\"2\",\"3\"]', '6500', '[\"12\",\"11\",\"7\",\"2\",\"9\",\"10\",\"4\",\"3\",\"8\",\"6\",\"5\"]', '[\"2\",\"9\",\"10\",\"4\",\"3\"]', '[\"3\"]', '[\"3\"]', '2017-05-31 05:08:56', '2017-06-04 09:26:57');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `password`, `data_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Вербицкая Маргарита Алексеевна', 'rita070597@gmail.com', '$2y$10$RQU6lIjXMbArvGZQstBUt.lJloAyNAb6mOGqh.og7TNFYzx1R6DTq', NULL, 'TssRirDD5e4gIEhVqMXKjy5Y81ahP5MONlnU0LwSm3iLNLAkFa4Vb6QCja2i', '2017-05-28 15:54:15', '2017-05-30 09:59:35'),
(2, 'client', 'Иван Иванович Иванов', 'ivan@mail.ru', '$2y$10$2ITLZbQzzEOL.cJ8p4IuSul5icgvig3pkAa6RYn6Jj8dhByfPbORy', 1, 'vVeixEWu9hlKdfgGlFWo4Q9QcpoFoSJ3FphzCihawsKwXs7uajejGkEPoKzI', '2017-06-02 05:45:12', '2017-06-02 05:45:12'),
(3, 'client', 'Пётр Петрович Петров', 'petr@mail.ru', '$2y$10$W0b.sBfW9I5Mt2cKRQwi8OvistbwYIp1vLw3Vayzp8rhSwMglfxPW', 2, 'L5c6x6Cbd7ECHpkRUFHsI5ho90VSiyWzLovYvdE7GN7dRVfaOZeA9oeiIQiU', '2017-06-02 09:38:31', '2017-06-02 09:38:31'),
(4, 'client', 'Юзер Юзерович Юзеров', 'margorksi@gmail.com', '$2y$10$w1aYfdY2bMkgJmfBmT3rwOO39TjlO6iIXLg.vhs2h6HaX2HEoyaTG', NULL, NULL, '2017-06-04 09:36:33', '2017-06-04 09:36:33'),
(5, 'client', 'Вербицкая Маргарита Алексеевна', 'margo0705rksi@gmail.com', '$2y$10$B/.zjQB3eSSoFDy16K05quW6XX5V2u1YeBZlkFw0OqZa83TmlbT1i', NULL, NULL, '2017-06-04 10:14:02', '2017-06-04 10:14:02');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `clients_data`
--
ALTER TABLE `clients_data`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `company_data`
--
ALTER TABLE `company_data`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `constructor_elements`
--
ALTER TABLE `constructor_elements`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `constructor_sections`
--
ALTER TABLE `constructor_sections`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `constructor_steps`
--
ALTER TABLE `constructor_steps`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `site_types`
--
ALTER TABLE `site_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `clients_data`
--
ALTER TABLE `clients_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `company_data`
--
ALTER TABLE `company_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `constructor_elements`
--
ALTER TABLE `constructor_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT для таблицы `constructor_sections`
--
ALTER TABLE `constructor_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `constructor_steps`
--
ALTER TABLE `constructor_steps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `site_types`
--
ALTER TABLE `site_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
