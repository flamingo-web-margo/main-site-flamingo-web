$(function() {
	$('.del').on('click', itemDelete);
	$('.add-field-desc').on('click', addFieldDesc);
	$('form[role=form]').on('click', '.del-field', delField);
	$('input[name^="elems_all"]').on('change', toogleCheckbox);
	$('input[name^="sections_all"]').on('change', toogleCheckbox);
	$('#performed-panel input[name="performed"]').on('change', changePerformed);
	$('[href^="#form-tab"]').on('click', changeType);
	selectType();
});

function itemDelete(event) {
	event.preventDefault();

	$parent = $(this).closest('tr');
	
	let rowspan = $parent.prevAll('.name').first().find('td:first-child').attr('rowspan');
	$parent.prevAll('.name').first().find('td:first-child').attr('rowspan', rowspan - 1);
	if ($parent.prev().is('tr.name')) $parent.prev('.name').remove();
	$parent.remove();

	
	console.log($(this).attr('href'));
	if (confirm('Уверены, что хотите удалить?')) {
		$.ajax({
			url:  $(this).attr('href'),
			method: 'POST',
			data: {
				'_token': $(this).attr('data-token'),
				'_method': 'DELETE'
			},
			success: function(msg) {
				$parent.remove();
				$('#alerts').append('<div class="alert alert-success">"' + msg + '" успешно удалён</div>');
			},
			error: function(msg) {
				console.log(msg);
			}
		});
	}
}

function addFieldDesc(event) {
	var $prevElem = $(this).prev('.field-desc');
	var number = +($prevElem.attr('data-number')) + 1;
	var $newField = $prevElem.clone().attr('data-number', number);

	$newField.children('label').attr('for', 'desc-f-' + number).text('Пункт №' + number);
	$newField.children('input').attr('id', 'desc-f-' + number).val('');
	$newField.insertBefore($(this));
}

function delField(event) {
	$(this).closest('.field-desc').remove();
}

function toogleCheckbox(event) {
	$default = $(this).closest('td').next().children('input');
	$block = $(this).closest('td').next().next().children('input');

	if ($default.prop('disabled')) {
		$default.prop('disabled', false);
	} else {
		$default.prop('checked', false);
		$default.prop('disabled', true);
	}

	if ($block) {
		if ($block.prop('disabled')) {
			$block.prop('disabled', false);
		} else {
			$block.prop('checked', false);
			$block.prop('disabled', true);
		}
	}
	
}

function selectType() {

	if ($('#select-type').find('option:selected').val() == 'Одностраничный') {
		$('[data-select-type="1"]').hide();
		$('[data-select-type="2"]').show();
	} else {
		$('[data-select-type="1"]').show();
		$('[data-select-type="2"]').hide();
	}

	$('#select-type').on('change', selectType);
	
}

function changePerformed(event) {
	let $performed = $(event.target).val();
	$('#performed-panel .progress-bar').attr('aria-valuenow', $performed).width($performed + '%').text($performed + '%');
}

function changeType(event) {
	$(event.target).closest('.panel').siblings().find('input[name="id_form[]"]').attr('checked', false);
	$(event.target).prev().attr('checked', true);
}