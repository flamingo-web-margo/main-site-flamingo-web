var order = {
	'stepNumber': 1, //номер шага, на котором находимся
	'sectionsAndElements': {}, //выбранные секции и элементы в них
};

var logotype = null; //логотип

$(function() {
	localStorage.clear();
	//обработчик на расфокусировку полей
	$('input, textarea').on('blur', filledField);

	//обработчик для табов и аккордиона на третьем шаге
	$('#section-order').on('click', '#elems-list', tabsElems);

	//обработчик на кнопки типов сайтов
	$('#section-order').on('click', 'button[data-type="type"]', getSections);
	//обработчик на кнопки разделов
	$('#section-order').on('click', 'button[data-type="section"]', getElements);
	//обработчик на поле с изображением
	$('#section-order').on('change', '#f-logo', logoUpload);
	//обработчик на отправку заказа
	$('#order-form').on('submit', orderDispatch);
	//обработчик на отправку индивидуального заказа
	$('#order-form-individual').on('submit', orderIndividual);
	
	//обработчик на элементы
	$('#section-order').on('change', '#elems-list input', changeElement);

	//проверка закончен ли предыдущий заказ
	//if (localStorage['idSiteConfig'] != null) loadLocalData();
});

//функция для обработки заполненных полей
function filledField(event) {
	if ($(this).val()) {
		$(this).addClass('filled')
	} else {
		$(this).removeClass('filled')
	}
}

//функция для табов и аккордиона на третьем шаге
function tabsElems(event) {
	var $target = $(event.target);

	if ($target.is('.elems-tab > li')) {
		$target.addClass('active').siblings('li').removeClass('active'); 
		$('#elems-list [data-tab]').hide();
		$('#elems-list [data-tab="' + $target.attr('data-name') + '"]').slideDown();
		return false;
	}

	if ($target.is('ul > li > span')) {
		var $elemSection = $target.closest('[data-tab] > li');
		$elemSection.toggleClass('active').siblings('li').removeClass('active').children('ol').slideUp();
		$elemSection.children('ol').slideToggle();

		if ($('#elems-mockup').attr('data-site-config-id') == 2) {
			$('[data-mockup-section-id=' + $elemSection.attr('data-section-id') + ']').slideToggle().siblings('figure').hide();
		}

		$('[data-section="elements"] #elems-mockup > p').hide();
		return false;
	}
}

//функция для изменения шага
function changeStep(step) {
	if (order.stepNumber == 6 && !localStorage['idSiteConfig']) {
		order.stepNumber = 1;
	} else {
		if (!localStorage['idSiteConfig'] && step != 6) return false;
	
		switch (step) {
			case 'prev':
				if (order.stepNumber == 1) return false;
				--order.stepNumber;
				break;
			case 'next':
				if (order.stepNumber == 5) return false;
				++order.stepNumber;
				break;
			case 6:
				order.stepNumber = 6;
				break;
			default:
				if (step < 1 || step > 6) return false;
				order.stepNumber = step;
				break;
		}
	}

	localStorage['stepNumber'] = order.stepNumber;
	$('#section-order').attr('data-open', order.stepNumber);
}

//функция загрузки изображения и отображения превью
function logoUpload(event) {
	var imgBox = $(this).next();
    logotype = this.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(logotype)
    console.log(1);
    $(reader).on('load', function(event) {

    	var result = event.originalEvent.target.result;
    	imgBox.css('background', 'transparent').html('<img src="' + result + '">');
    });
    
}

function getSections(event = null, idSiteConfig = false) {
	var $target = $(event.target);
	var value = idSiteConfig ? idSiteConfig : $target.attr('data-value');

	$.ajax({
		url: '/main-site-flamingo-web/order',
		method: 'POST',
		data: {
			'_token': $('#section-order').attr('data-token'),
			'type': 'sections',
			'idSiteConfig': value
		},
		success: function(response) {
			$('[data-section="sections"]').html(response.view);
			localStorage['idSiteConfig'] = value;
			localStorage['sectionsAndElements'] = response.config;
			getElements(true);
			changeStep('next');
			$('[data-sitetype-id="' + value + '"]').addClass('active').siblings('article').removeClass('active');
			$('#section-order').removeClass('navigation-none');

		},
		error: function(msg) {
			console.log(msg);
		}
	});


}

function getElements(event = null, first = false) {

	console.log(localStorage['sectionsAndElements']);
	if (!first) {
		var target = $(event.target);
		var idSection = target.attr('data-value');

		var sectionsAndElements = JSON.parse(localStorage['sectionsAndElements']);

		if (target.closest('article').hasClass('active')) {
			target.closest('article').removeClass('active');
			target.text('Добавить');
			delete sectionsAndElements[idSection];
		} else {
			target.closest('article').addClass('active');
			target.text('Отменить');
			sectionsAndElements[idSection] = [];
		}
		localStorage['sectionsAndElements'] = JSON.stringify(sectionsAndElements);
	}

	$.ajax({
		url: '/main-site-flamingo-web/order',
		method: 'POST',
		data: {
			'_token': $('#section-order').attr('data-token'),
			'idSiteConfig': localStorage['idSiteConfig'],
			'type': 'elements',
			'sectionsAndElements': localStorage['sectionsAndElements'],
			'first': first
		},
		success: function(response) {

			$('[data-section="elements"]').html(response.view);
			setPrice(response.price);
			localStorage['sectionsAndElements'] = response.config;
			//формирование макета
			constructorMockup();
		},
		error: function(msg) {
			console.log(msg);
		}
	});
}

//функция формирование макета
function constructorMockup() {
	if ($('#elems-mockup').attr('data-site-config-id') == 2) {
		$('[data-mockup-section-id]').hide();
		$('[data-mockup-section-id="0"]').show();
	}
}
	

function changeElement() {
	var sectionsAndElements = JSON.parse(localStorage['sectionsAndElements']);
	var section = $(event.target).closest('[data-section-id]').attr('data-section-id');
	var elem = $(event.target).val();

	if (section == 0) {
		switch ($(event.target).attr('name')) {
			case 'header':
				sectionsAndElements[0][0] = +elem;
			break;
			case 'footer':
				sectionsAndElements[0][1] = +elem;
			break;
		}
		$('[data-mockup-section-id="0"] [data-mockup-elem-id=' + elem + ']').fadeIn().siblings().hide();
	} else {

	if (sectionsAndElements[section].indexOf(elem) != -1) {
		sectionsAndElements[section].splice(sectionsAndElements[section].indexOf(elem), 1);
		$('[data-mockup-section-id=' + section + '] [data-mockup-elem-id=' + elem + ']').slideUp();
	} else {
		sectionsAndElements[section].push(elem);
		$('[data-mockup-section-id=' + section + '] [data-mockup-elem-id=' + elem + ']').slideDown();
	}
}

	localStorage['sectionsAndElements'] = JSON.stringify(sectionsAndElements);


	$.ajax({
		url: '/main-site-flamingo-web/order',
		method: 'POST',
		data: {
			'_token': $('#section-order').attr('data-token'),
			'idSiteConfig': localStorage['idSiteConfig'],
			'type': 'elements',
			'sectionsAndElements': localStorage['sectionsAndElements']
		},
		success: function(response) {
			setPrice(response.price);
		},
		error: function(msg) {
			console.log(msg);
		}
	});
}

function setPrice(price) {
	var maxPrice = 80000;
	var heightScale = $('#order-price div').height();
	var heightPrice = 100 * price / maxPrice;


	$('#order-price div').height(heightPrice + '%');
	$('#order-price span').text(price + 'руб').css({'bottom': heightPrice + '%', 'opacity': '1'});
}

//функция для работы с локальными данными о заказе
function loadLocalData() {
	var localData = localStorage;
	//console.log(localData);

	if (confirm('Вы не завершили предыдущий заказ. Хотите восстановить данные?')) {
		getSections(false, localData['idSiteConfig']);
		changeStep(localData['stepNumber']);
		localStorage['sectionsAndElements'] = localData['sectionsAndElements'];
		getElements(null, true);
	} else {
		localStorage.clear();
	}
}

//функция отправки заказа
function orderDispatch(event) {
	event.preventDefault();
	var formData = new FormData($(this)[0]);

	formData.append('id_site_config', localStorage['idSiteConfig']);
	formData.append('detail_config', localStorage['sectionsAndElements']);
	formData.append('logotype', logotype);
	formData.append('type', 'order');

	$.ajax({
		url: '/main-site-flamingo-web/order',
		method: 'POST',
		data: formData,
		processData: false,
		contentType: false,
		success: function(response) {
			$('[data-section="set-order"]').html(response.view);
			localStorage.clear();
		},
		error: function(response) {
			console.log(response);
		}
	});
}

//функция отправки заказа
function orderIndividual(event) {
	event.preventDefault();
	let $form = $(this);
	let formData = new FormData($form[0]);

	$.ajax({
		url: $form.attr('action'),
		method: 'POST',
		data: formData,
		processData: false,
		contentType: false,
		success: function(response) {
			$form.find('p').text(response.msg);
			if (response.type == 'success') {
				$form[0].reset();
				$form.find('.filled').removeClass('filled');
			}
		},
		error: function(response) {
			console.log(response);
		}
	});
}