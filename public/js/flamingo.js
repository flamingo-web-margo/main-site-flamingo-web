$(function() {
	performedLoad();
	$('main input, main textarea').each(filledField);
	//обработчик на расфокусировку полей
	$('input, textarea').on('blur', filledField);
	$('.sendMessage').on('submit', sendMessage);
	$('.nav-btn').on('click', function() {
		$(this).closest('header').toggleClass('nav-active');
	});
});

//функция для обработки заполненных полей
function filledField(event) {

	if ($(this).val()) {
		$(this).addClass('filled');
	} else {
		$(this).removeClass('filled');
	}
}

function performedLoad() {

	$('.performed').each(function() {
		if ($(this).find('span').text() != '0%') {
			$(this).find('div').width($(this).find('span').text());
			$(this).find('span').css({'left': $(this).find('span').text(),
										'opacity': '1',
										'width': '40px',
										'margin-left': '-20px'
									});
		} else {
			$(this).find('span').css({'opacity': '1'});
		}
	});
}

//функция отправки сообщения
function sendMessage(event) {
	event.preventDefault();
	let $form = $(this);
	let formData = new FormData($form[0]);

	$.ajax({
		url: $form.attr('action'),
		method: 'POST',
		data: formData,
		processData: false,
		contentType: false,
		success: function(response) {
			$form.find('p').text(response.msg);
			if (response.type == 'success') {
				$form[0].reset();
				$form.find('.filled').removeClass('filled');
			}
		},
		error: function(response) {
			console.log(response);
		}
	});
}